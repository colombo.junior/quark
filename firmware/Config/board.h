#ifndef BOARD_BOARD_H_
#define BOARD_BOARD_H_

#include "stm32f1xx_hal.h"
#include "tim.h"

// Available hardware on this board
#define NUMBER_OF_ENCODERS						2
#define NUMBER_OF_ADS							4
#define NUMBER_OF_DAS							0
#define NUMBER_OF_DIGITAL_INPUTS				4
#define NUMBER_OF_DIGITAL_OUTPUTS				4
#define NUMBER_OF_PWM							4
#define NUMBER_OF_ONEWIRE						3
#define NUMBER_OF_SPI							1
#define NUMBER_OF_I2C							1


// Clock definition
#define F_CPU									SystemCoreClock
#define BOARD_CLOCK_TIMER						(htim1)
#define BOARD_CLOCK_RESOLUTION_US				1
#define BOARD_CLOCK_OVERFLOW_US					1000


// Encoder definitions
#define ENCODER_1								0
#define ENCODER_2								1
#define ENC1_CHA_EXTI_IRQ						EXTI4_IRQn
#define ENC1_CHB_EXTI_IRQ						EXTI9_5_IRQn
#define ENC2_CHA_EXTI_IRQ						EXTI15_10_IRQn
#define ENC2_CHB_EXTI_IRQ						EXTI15_10_IRQn
#define ENC_TIMER                               (htim1)
#define ENC_TIMER_OVERFLOW_US                   1000
constexpr uint32_t ENCODER_PREEMPT_PRIORITY =	0; ///< Maximum priority
constexpr uint32_t ENCODER_SUB_PRIORITY =		0; ///< Maximum priority
constexpr int32_t ENC_DEFAULT_TIMEOUT_MS =		100;
constexpr int32_t ENC_DEFAULT_TIMEOUT_US =		1000 * ENC_DEFAULT_TIMEOUT_MS;
constexpr uint8_t ENC_DEFAULT_FILTER_SIZE =     4;
constexpr uint8_t ENC_BUFFER_SIZE =             32;
constexpr float ENC_DEFAULT_MAX_RATE =          0.1f;


// GPIO definitions
constexpr uint32_t GPIO_IN_PREEMPT_PRIORITY =   0;
constexpr uint32_t GPIO_IN_SUB_PRIORITY =       1;
#define GPIO_IN_CH1_IRQ                         EXTI9_5_IRQn
#define GPIO_IN_CH2_IRQ                         EXTI15_10_IRQn
#define GPIO_IN_CH3_IRQ                         EXTI15_10_IRQn
#define GPIO_IN_CH4_IRQ                         EXTI15_10_IRQn


// PWM definitions
#define BOARD_PWM_TIMER							htim3
#define PWM_CH1                     			TIM_CHANNEL_1
#define PWM_CH2                     			TIM_CHANNEL_2
#define PWM_CH3                     			TIM_CHANNEL_3
#define PWM_CH4                     			TIM_CHANNEL_4
constexpr float PWM_MIN_FREQUENCY =				0.02;
constexpr float PWM_MAX_FREQUENCY =				720000;
constexpr float PWM_MIN_DUTY =					0;
constexpr float PWM_MAX_DUTY =					1;


// Communication definitions
constexpr uint8_t BOARD_ID = 					0x40;
constexpr uint8_t BROADCAST_ID = 				0xFE;
constexpr uint8_t TIMEOUT_COMMUNICATION_MS =	2;
constexpr uint16_t MEMORY_TABLE_SIZE =			128;


// Board information registers
constexpr uint8_t VERSION_MAJOR =				1;
constexpr uint8_t VERSION_MINOR =				1;
constexpr uint8_t VERSION_PATCH =				1;
constexpr uint8_t WHO_AM_I_VALUE = 				0x1D;	// my age (in years) at the beginning of the board development


#endif /* BOARD_BOARD_H_ */
