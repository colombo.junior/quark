#include "memory_table.h"

namespace memory_table {

MemoryTable::MemoryTable()
{
	// Updates the firmware version and WHO_AM_I register
	write_byte(Register::VERSION_MAJOR, VERSION_MAJOR);
	write_byte(Register::VERSION_MINOR, VERSION_MINOR);
	write_byte(Register::VERSION_PATCH, VERSION_PATCH);
	write_byte(Register::WHO_AM_I, WHO_AM_I_VALUE);
}

void MemoryTable::clean()
{
	uint16_t i;
	for (i = 0; i < MEMORY_TABLE_SIZE; i++)
		data[i] = 0;
}

void MemoryTable::begin()
{
	write_byte(Register::GPIO_INPUT_STATE, static_cast<uint8_t>(InitialValue::GPIO_INPUT_STATE));
	write_byte(Register::GPIO_INPUT_PULL, static_cast<uint8_t>(InitialValue::GPIO_INPUT_PULL));

	write_float(Register::PWM_FREQUENCY, static_cast<float>(InitialValue::PWM_FREQUENCY));

	write_byte(Register::ENC_1_CONFIG, static_cast<uint8_t>(InitialValue::ENC_1_CONFIG));
	write_byte(Register::ENC_2_CONFIG, static_cast<uint8_t>(InitialValue::ENC_2_CONFIG));
	write_byte(Register::ENC_1_TIMEOUT_MS, static_cast<uint8_t>(InitialValue::ENC_1_TIMEOUT_MS));
	write_byte(Register::ENC_2_TIMEOUT_MS, static_cast<uint8_t>(InitialValue::ENC_2_TIMEOUT_MS));
	write_dword(Register::ENC_1_TIME, static_cast<int32_t>(InitialValue::ENC_1_TIME));
	write_dword(Register::ENC_2_TIME, static_cast<int32_t>(InitialValue::ENC_2_TIME));

}

void MemoryTable::write_byte(Register reg, uint8_t value)
{
	data[static_cast<uint8_t>(reg)] = value;
}

uint8_t MemoryTable::read_byte(Register reg)
{
	return data[static_cast<uint8_t>(reg)];
}

uint8_t MemoryTable::read_byte(uint8_t reg)
{
	return data[reg];
}

void MemoryTable::write_word(Register reg, uint16_t value)
{
	uint16_t* reg_on_table = (uint16_t*) &data[static_cast<uint8_t>(reg)];
	*reg_on_table = value;
}

uint16_t MemoryTable::read_word(Register reg)
{
	uint16_t* value = reinterpret_cast<uint16_t*>(&data[static_cast<uint8_t>(reg)]);
	return *value;
}

void MemoryTable::write_dword(Register reg, uint32_t value)
{
	uint32_t* reg_on_table = (uint32_t*) &data[static_cast<uint8_t>(reg)];
	*reg_on_table = value;
}

void MemoryTable::write_float(Register reg, float value)
{
	float* reg_on_table = (float*) &data[static_cast<uint8_t>(reg)];
	*reg_on_table = value;
}

float MemoryTable::read_float(Register reg)
{
	float* reg_on_table = (float*) &data[static_cast<uint8_t>(reg)];
	float value = *reg_on_table;
	return value;
}

bool test_read_write(MemoryTable* mt)
{
	uint8_t ok = true;

	mt->write_byte(Register::GPIO_INPUT_STATE, 0x0A);
	uint8_t a = mt->read_byte(Register::GPIO_INPUT_STATE);
	if (a != 0x0A)
		ok = false;

	mt->write_word(Register::GPIO_INPUT_PULL, 0x0A0A);
	uint16_t b = mt->read_word(Register::GPIO_INPUT_PULL);
	if (b != 0x0A0A)
		ok = false;

	mt->write_float(Register::GPIO_OUTPUT_STATE, 3.1415);
	float c = mt->read_float(Register::GPIO_OUTPUT_STATE);
	if (((c - 3.1415) > 1e-6) || ((c - 3.1415) < 1e-6))
		ok = false;

	return ok;
}

} // namespace memory_table


