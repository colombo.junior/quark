#ifndef MANAGER_MANAGER_H_
#define MANAGER_MANAGER_H_

/**
  ******************************************************************************
  * @file    manager.h
  * @author  José Roberto Colombo Junior
  * @brief   Header file of the manager
  ******************************************************************************
  * @attention
  *
  * This is free software licensed under GPL.
  *
  ******************************************************************************
  */

#include <stdint.h>

#include "main.h"

#include "Config/board.h"
#include "memory_table.h"
#include "Drivers/communication/messenger.h"
#include "Drivers/communication/protocol.h"
#include "Drivers/hal/pwm.h"
#include "Drivers/encoder/encoder.h"

enum class ManagerError : uint8_t {
	NONE = 0x00,
	UNWRITABLE_REGISTER = 0x01,
	UNREADABLE_REGISTER = 0x02,
	UNSUPPORTED_FEATURE = 0x03,
};

class Manager {
	private:
		memory_table::MemoryTable* mt;
		messenger::Messenger* msgr;
		encoder::Encoder* encs;

		bool adc_enable;
		bool adc_use_filter;

	protected:
	public:
		Manager();
		~Manager();

		void set_memory_table_address(memory_table::MemoryTable* mt);
		void set_usb_messenger_address(messenger::Messenger* usb_msgr);
		void set_encoders_address(encoder::Encoder* address);
		void begin_gpio();

		void process_instruction_packet_for_me();
		void process_instruction_packet_for_broadcast();
		void deal_with_read_command();
		ManagerError deal_with_write_command();

		void make_response_for_ping();
		void make_response_for_read(uint8_t what_register, uint8_t length);
		void make_response_for_invalid_instruction();
		void make_response_for_write();

		void gpio_read();
		void gpio_write(uint8_t requested_output);
		uint8_t gpio_get_pull_from_st(uint8_t pull);

		uint8_t gpio_get_mode_from_st(uint8_t mode);
		void gpio_update_input_config();
		void gpio_update_output_config();

		bool is_adc_enabled();
		bool is_adc_filter_enabled();
		void enable_adc();
		void disable_adc();
		void adc_read(uint32_t* adc_buffer);
		void adc_calculate_filter_coefficients();

		void update_encoder_config(uint8_t config, encoder::Encoder* enc);
		void update_encoder_readings_in_mt(uint8_t what_encoder);
};



#endif /* MANAGER_MANAGER_H_ */
