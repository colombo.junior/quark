#include "manager.h"

Manager::Manager()
{
	adc_enable = false;
	adc_use_filter = false;
}

void Manager::set_memory_table_address(memory_table::MemoryTable* mt)
{
	this->mt = mt;
	mt->begin();
}

void Manager::set_usb_messenger_address(messenger::Messenger* usb_msgr)
{
	this->msgr = usb_msgr;
}

void Manager::set_encoders_address(encoder::Encoder* address)
{
	this->encs = address;

	uint8_t i;
	for (i = ENCODER_1; i < NUMBER_OF_ENCODERS; i++)
	{
		encs[i].begin(i);
	}
}

void Manager::begin_gpio()
{
    // The GPIO intput must be configured as
    // - INPUT
    // - with INTERRUPT capturing rising/falling events
    // - with PULLUP resistor
    GPIO_InitTypeDef gpio;
    gpio.Pull = GPIO_PULLUP;
    gpio.Mode = GPIO_MODE_IT_RISING_FALLING;
    gpio.Speed = GPIO_SPEED_FREQ_HIGH;

    // De-init the input GPIO pins
    HAL_GPIO_DeInit(GPIO_IN_CH1_GPIO_Port, GPIO_IN_CH1_Pin);
    HAL_GPIO_DeInit(GPIO_IN_CH2_GPIO_Port, GPIO_IN_CH2_Pin);
    HAL_GPIO_DeInit(GPIO_IN_CH3_GPIO_Port, GPIO_IN_CH3_Pin);
    HAL_GPIO_DeInit(GPIO_IN_CH4_GPIO_Port, GPIO_IN_CH4_Pin);

    // Init the GPIO INPUT pins
    gpio.Pin = GPIO_IN_CH1_Pin;
    HAL_GPIO_Init(GPIO_IN_CH1_GPIO_Port, &gpio);
    HAL_NVIC_SetPriority(GPIO_IN_CH1_IRQ, GPIO_IN_PREEMPT_PRIORITY, GPIO_IN_SUB_PRIORITY);
    HAL_NVIC_EnableIRQ(GPIO_IN_CH1_IRQ);

    gpio.Pin = GPIO_IN_CH2_Pin;
    HAL_GPIO_Init(GPIO_IN_CH2_GPIO_Port, &gpio);
    HAL_NVIC_SetPriority(GPIO_IN_CH2_IRQ, GPIO_IN_PREEMPT_PRIORITY, GPIO_IN_SUB_PRIORITY);
    HAL_NVIC_EnableIRQ(GPIO_IN_CH2_IRQ);

    gpio.Pin = GPIO_IN_CH3_Pin;
    HAL_GPIO_Init(GPIO_IN_CH3_GPIO_Port, &gpio);
    HAL_NVIC_SetPriority(GPIO_IN_CH3_IRQ, GPIO_IN_PREEMPT_PRIORITY, GPIO_IN_SUB_PRIORITY);
    HAL_NVIC_EnableIRQ(GPIO_IN_CH3_IRQ);

    gpio.Pin = GPIO_IN_CH4_Pin;
    HAL_GPIO_Init(GPIO_IN_CH4_GPIO_Port, &gpio);
    HAL_NVIC_SetPriority(GPIO_IN_CH4_IRQ, GPIO_IN_PREEMPT_PRIORITY, GPIO_IN_SUB_PRIORITY);
    HAL_NVIC_EnableIRQ(GPIO_IN_CH4_IRQ);

}

Manager::~Manager()
{
	mt = nullptr;
	msgr = nullptr;
	encs = nullptr;
}

void Manager::process_instruction_packet_for_me()
{
	if (msgr->is_valid == true)
	{
		switch (msgr->instruction_packet.get_command())
		{
		case protocol::Command::PING:
			make_response_for_ping();
			msgr->send_packet(&msgr->status_packet);
			break;

		case protocol::Command::READ:
			deal_with_read_command();
			msgr->send_packet(&msgr->status_packet);
			break;

		case protocol::Command::WRITE:
			deal_with_write_command();
			make_response_for_write();
			msgr->send_packet(&msgr->status_packet);
			break;

		case protocol::Command::REBOOT:
			HAL_NVIC_SystemReset();
			break;

		default:
			break;
		}
	}

	else // if the message is not valid
	{
//				manager.make_response_packet_for_invalid_instruction_packet();
//				CDC_Transmit_FS(usb.status_packet., Len);
	}


}

void Manager::process_instruction_packet_for_broadcast()
{
	if (msgr->is_valid == true)
	{
		switch (msgr->instruction_packet.get_command())
		{
		case protocol::Command::WRITE:
			deal_with_write_command();
			// WRITE commands in broadcast do not generate status packet
			break;

		case protocol::Command::REBOOT:
			HAL_NVIC_SystemReset();
			break;

		default:
			break;
		}
	}
}

void Manager::deal_with_read_command()
{
	uint8_t first_register = msgr->instruction_packet.get_parameter(0);
	uint8_t length = msgr->instruction_packet.get_parameter(1);
	make_response_for_read(first_register, length);
}

ManagerError Manager::deal_with_write_command()
{
	using namespace memory_table;

	ManagerError error = ManagerError::NONE;

	uint8_t* parameters = msgr->instruction_packet.get_addr_of_parameters();
	Register reg = (Register) parameters[0];
	uint8_t* values = &parameters[1];

	// Auxiliary variables
	uint16_t* adc_filter_freq;
	float* new_duty;
	float* new_freq;

	switch (reg)
	{
		case Register::GPIO_INPUT_STATE:
			error = ManagerError::UNWRITABLE_REGISTER;
			break;

		case Register::GPIO_INPUT_PULL:
			mt->write_byte(Register::GPIO_INPUT_PULL, values[0]);
			gpio_update_input_config();
			break;

		case Register::GPIO_OUTPUT_STATE:
			mt->write_byte(Register::GPIO_OUTPUT_STATE, values[0]);
			gpio_write(values[0]);
			break;

		case Register::GPIO_OUTPUT_PULL:
			mt->write_byte(Register::GPIO_OUTPUT_PULL, values[0]);
			gpio_update_output_config();
			break;

		case Register::GPIO_OUTPUT_MODE:
			mt->write_byte(Register::GPIO_OUTPUT_MODE, values[0]);
			gpio_update_output_config();
			break;

		case Register::ADC_CONFIG:
			mt->write_byte(Register::ADC_CONFIG, values[0]);
			adc_enable = is_adc_enabled();
			break;

		case Register::ADC_FILTER_FREQ:
			adc_filter_freq = (uint16_t*) values;
			mt->write_word(Register::ADC_FILTER_FREQ, *adc_filter_freq);

			adc_use_filter = is_adc_filter_enabled();
			if (adc_use_filter == true)
				adc_calculate_filter_coefficients();
			break;

		case Register::PWM_FREQUENCY:
			new_freq = (float*) values;

			if (*new_freq >= PWM_MIN_FREQUENCY && *new_freq <= PWM_MAX_FREQUENCY)
			{
				mt->write_float(Register::PWM_FREQUENCY, *new_freq);
				set_pwm_frequency(&(BOARD_PWM_TIMER), *new_freq);
			}
			break;

		case Register::PWM_DUTY_CH1:
			new_duty = (float*) values;

			if (*new_duty >= PWM_MIN_DUTY && *new_duty <= PWM_MAX_DUTY)
			{
				mt->write_float(Register::PWM_DUTY_CH1, *new_duty);
				set_duty_ch1(*new_duty);
			}
			break;

		case Register::PWM_DUTY_CH2:
			new_duty = (float*) values;

			if (*new_duty >= PWM_MIN_DUTY && *new_duty <= PWM_MAX_DUTY)
			{
				mt->write_float(Register::PWM_DUTY_CH2, *new_duty);
				set_duty_ch2(*new_duty);
			}
			break;

		case Register::PWM_DUTY_CH3:
			new_duty = (float*) values;

			if (*new_duty >= PWM_MIN_DUTY && *new_duty <= PWM_MAX_DUTY)
			{
				mt->write_float(Register::PWM_DUTY_CH3, *new_duty);
				set_duty_ch3(*new_duty);
			}
			break;

		case Register::PWM_DUTY_CH4:
			new_duty = (float*) values;

			if (*new_duty >= PWM_MIN_DUTY && *new_duty <= PWM_MAX_DUTY)
			{
				mt->write_float(Register::PWM_DUTY_CH4, *new_duty);
				set_duty_ch4(*new_duty);
			}
			break;

		case Register::ENC_1_CONFIG:
			mt->write_byte(Register::ENC_1_CONFIG, values[0]);
			update_encoder_config(values[0], &encs[ENCODER_1]);
			break;

		case Register::ENC_2_CONFIG:
			mt->write_byte(Register::ENC_2_CONFIG, values[0]);
			update_encoder_config(values[0], &encs[ENCODER_2]);
			break;

        case Register::ENC_1_FILTER_SIZE:
            mt->write_byte(Register::ENC_1_FILTER_SIZE, values[0]);
            encs[ENCODER_1].set_filter_size(values[0]);
            break;

        case Register::ENC_2_FILTER_SIZE:
            mt->write_byte(Register::ENC_2_FILTER_SIZE, values[0]);
            encs[ENCODER_2].set_filter_size(values[0]);
            break;

        case Register::ENC_1_REL_MAX_RATE:
            mt->write_byte(Register::ENC_1_REL_MAX_RATE, values[0]);
            encs[ENCODER_1].set_max_rate(values[0]);
            break;

        case Register::ENC_2_REL_MAX_RATE:
            mt->write_byte(Register::ENC_2_REL_MAX_RATE, values[0]);
            encs[ENCODER_2].set_max_rate(values[0]);
            break;

		case Register::ENC_1_TIMEOUT_MS:
			mt->write_byte(Register::ENC_1_TIMEOUT_MS, values[0]);
			encs[ENCODER_1].set_timeout((uint32_t) 1000 * values[0]);
			break;

		case Register::ENC_2_TIMEOUT_MS:
			mt->write_byte(Register::ENC_2_TIMEOUT_MS, values[0]);
			encs[ENCODER_2].set_timeout((uint32_t) 1000 * values[0]);
			break;

		default:
			break;
		}

	return error;
}

void Manager::make_response_for_ping()
{
	msgr->status_packet.create_new_msg();
	msgr->status_packet.update_checksum_value();
}

void Manager::make_response_for_read(uint8_t what_register, uint8_t length)
{
	msgr->status_packet.create_new_msg();

	uint8_t i;
	for (i = 0; i < length; i++)
		msgr->status_packet.append_parameter(mt->read_byte(what_register + i));

	msgr->status_packet.update_checksum_value();
}

void Manager::make_response_for_invalid_instruction()
{
	msgr->status_packet.create_new_msg();

	if (!msgr->instruction_packet.is_checksum_valid())
		msgr->status_packet.append_error(protocol::Error::INVALID_CHECKSUM);

	if (!msgr->instruction_packet.is_command_valid())
		msgr->status_packet.append_error(protocol::Error::INVALID_INSTRUCTION);

	msgr->status_packet.update_checksum_value();
}

void Manager::make_response_for_write()
{
	msgr->status_packet.create_new_msg();
	msgr->status_packet.update_checksum_value();
}

void Manager::gpio_read()
{
	using namespace memory_table;

    uint8_t state = 0, ich1, ich2, ich3, ich4;

    ich1 = ((uint8_t) HAL_GPIO_ReadPin(GPIO_IN_CH1_GPIO_Port, GPIO_IN_CH1_Pin)) << static_cast<uint8_t>(POSITIONS_GPIO_INPUT_STATE::CH1);
    ich2 = ((uint8_t) HAL_GPIO_ReadPin(GPIO_IN_CH2_GPIO_Port, GPIO_IN_CH2_Pin)) << static_cast<uint8_t>(POSITIONS_GPIO_INPUT_STATE::CH2);
    ich3 = ((uint8_t) HAL_GPIO_ReadPin(GPIO_IN_CH3_GPIO_Port, GPIO_IN_CH3_Pin)) << static_cast<uint8_t>(POSITIONS_GPIO_INPUT_STATE::CH3);
    ich4 = ((uint8_t) HAL_GPIO_ReadPin(GPIO_IN_CH4_GPIO_Port, GPIO_IN_CH4_Pin)) << static_cast<uint8_t>(POSITIONS_GPIO_INPUT_STATE::CH4);
	state = state | ich1 | ich2 | ich3 | ich4;

	mt->write_byte(Register::GPIO_INPUT_STATE, state);
}

void Manager::gpio_write(uint8_t requested_output)
{
	using namespace memory_table;

    if ((requested_output & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_STATE::CH1_MASK)) > 0)
    {
    	HAL_GPIO_WritePin(GPIO_OUT_CH1_GPIO_Port, GPIO_OUT_CH1_Pin, GPIO_PIN_SET);
    }
    else
    {
    	HAL_GPIO_WritePin(GPIO_OUT_CH1_GPIO_Port, GPIO_OUT_CH1_Pin, GPIO_PIN_RESET);
    }

    if ((requested_output & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_STATE::CH2_MASK)) > 0)
    {
    	HAL_GPIO_WritePin(GPIO_OUT_CH2_GPIO_Port, GPIO_OUT_CH2_Pin, GPIO_PIN_SET);
    }
    else
    {
    	HAL_GPIO_WritePin(GPIO_OUT_CH2_GPIO_Port, GPIO_OUT_CH2_Pin, GPIO_PIN_RESET);
    }

	if ((requested_output & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_STATE::CH3_MASK)) > 0)
	{
		HAL_GPIO_WritePin(GPIO_OUT_CH3_GPIO_Port, GPIO_OUT_CH3_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIO_OUT_CH3_GPIO_Port, GPIO_OUT_CH3_Pin, GPIO_PIN_RESET);
	}

	if ((requested_output & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_STATE::CH4_MASK)) > 0)
	{
		HAL_GPIO_WritePin(GPIO_OUT_CH4_GPIO_Port, GPIO_OUT_CH4_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIO_OUT_CH4_GPIO_Port, GPIO_OUT_CH4_Pin, GPIO_PIN_RESET);
	}
}

uint8_t Manager::gpio_get_pull_from_st(uint8_t pull)
{
	using namespace memory_table;
	uint8_t st_pull;

	switch (pull) {
		case static_cast<uint8_t>(GPIO_PULL::NONE):
			st_pull = GPIO_NOPULL;
			break;

		case static_cast<uint8_t>(GPIO_PULL::DOWN):
			st_pull = GPIO_PULLDOWN;
			break;

		case static_cast<uint8_t>(GPIO_PULL::UP):
			st_pull = GPIO_PULLUP;
			break;

		default:
			st_pull = GPIO_PULLUP;
			break;
	}

	return st_pull;
}

uint8_t Manager::gpio_get_mode_from_st(uint8_t mode)
{
	using namespace memory_table;

	uint8_t st_mode = GPIO_MODE_OUTPUT_PP;

	if (mode == static_cast<uint8_t>(GPIO_OUTPUT_MODE::OPEN_DRAIN))
		st_mode = GPIO_MODE_OUTPUT_OD;

	return st_mode;
}

void Manager::gpio_update_input_config()
{
	using namespace memory_table;

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;

	// Read the configuration in memory table
	uint8_t pull_config = mt->read_byte(Register::GPIO_INPUT_PULL);

	// Extract the pull configurations
	uint8_t pull_config_ch1 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH1_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH1);
	uint8_t pull_config_ch2 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH2_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH2);
	uint8_t pull_config_ch3 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH3_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH3);
	uint8_t pull_config_ch4 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH4_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_INPUT_PULL::CH4);

	// Update input pin 1
	GPIO_InitStruct.Pull = pull_config_ch1;
	GPIO_InitStruct.Pin = GPIO_IN_CH1_Pin;
	HAL_GPIO_Init(GPIO_IN_CH1_GPIO_Port, &GPIO_InitStruct);

	// Update input pin 2
	GPIO_InitStruct.Pull = pull_config_ch2;
	GPIO_InitStruct.Pin = GPIO_IN_CH2_Pin;
	HAL_GPIO_Init(GPIO_IN_CH2_GPIO_Port, &GPIO_InitStruct);

	// Update input pin 3
	GPIO_InitStruct.Pull = pull_config_ch3;
	GPIO_InitStruct.Pin = GPIO_IN_CH3_Pin;
	HAL_GPIO_Init(GPIO_IN_CH3_GPIO_Port, &GPIO_InitStruct);

	// Update input pin 4
	GPIO_InitStruct.Pull = pull_config_ch4;
	GPIO_InitStruct.Pin = GPIO_IN_CH4_Pin;
	HAL_GPIO_Init(GPIO_IN_CH4_GPIO_Port, &GPIO_InitStruct);
}

void Manager::gpio_update_output_config()
{
	using namespace memory_table;

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

	// Read the configuration in memory table
	uint8_t pull_config = mt->read_byte(Register::GPIO_OUTPUT_PULL);
	uint8_t mode_config = mt->read_byte(Register::GPIO_OUTPUT_MODE);

	// Extract the pull configurations
	uint8_t pull_config_ch1 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH1_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH1);
	uint8_t pull_config_ch2 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH2_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH2);
	uint8_t pull_config_ch3 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH3_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH3);
	uint8_t pull_config_ch4 = (pull_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH4_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_PULL::CH4);

	// Extract the mode configurations
	uint8_t mode_config_ch1 = (mode_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH1_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH1);
	uint8_t mode_config_ch2 = (mode_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH2_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH2);
	uint8_t mode_config_ch3 = (mode_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH3_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH3);
	uint8_t mode_config_ch4 = (mode_config & static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH4_MASK)) >> static_cast<uint8_t>(POSITIONS_GPIO_OUTPUT_MODE::CH4);

	// Update output pin 1
	GPIO_InitStruct.Mode = gpio_get_mode_from_st(mode_config_ch1);
	GPIO_InitStruct.Pull = pull_config_ch1;
	GPIO_InitStruct.Pin = GPIO_OUT_CH1_Pin;
	HAL_GPIO_Init(GPIO_OUT_CH1_GPIO_Port, &GPIO_InitStruct);

	// Update output pin 2
	GPIO_InitStruct.Mode = gpio_get_mode_from_st(mode_config_ch2);
	GPIO_InitStruct.Pull = pull_config_ch2;
	GPIO_InitStruct.Pin = GPIO_OUT_CH2_Pin;
	HAL_GPIO_Init(GPIO_OUT_CH2_GPIO_Port, &GPIO_InitStruct);

	// Update output pin 3
	GPIO_InitStruct.Mode = gpio_get_mode_from_st(mode_config_ch3);
	GPIO_InitStruct.Pull = pull_config_ch3;
	GPIO_InitStruct.Pin = GPIO_OUT_CH3_Pin;
	HAL_GPIO_Init(GPIO_OUT_CH3_GPIO_Port, &GPIO_InitStruct);

	// Update output pin 4
	GPIO_InitStruct.Mode = gpio_get_mode_from_st(mode_config_ch4);
	GPIO_InitStruct.Pull = pull_config_ch4;
	GPIO_InitStruct.Pin = GPIO_OUT_CH4_Pin;
	HAL_GPIO_Init(GPIO_OUT_CH4_GPIO_Port, &GPIO_InitStruct);
}

bool Manager::is_adc_enabled()
{
	using namespace memory_table;

	uint8_t adc_config = mt->read_byte(Register::ADC_CONFIG);
	return (adc_config & static_cast<uint8_t>(POSITIONS_ADC_CONFIG::ENABLE_MASK)) >> static_cast<uint8_t>(POSITIONS_ADC_CONFIG::ENABLE);
}

bool Manager::is_adc_filter_enabled()
{
	using namespace memory_table;

	uint16_t filter_freq = mt->read_word(Register::ADC_FILTER_FREQ);
	return filter_freq > 0;
}


void Manager::enable_adc()
{
	using namespace memory_table;
	mt->write_byte(Register::ADC_CONFIG, (uint8_t) POSITIONS_ADC_CONFIG::ENABLE_MASK);
}

void Manager::disable_adc()
{
	using namespace memory_table;
	mt->write_byte(Register::ADC_CONFIG, 0);
}

void Manager::adc_read(uint32_t* adc_buffer)
{
	using namespace memory_table;

//	if (adc_use_filter == true)
//	{
//	}

	mt->write_word(Register::ADC_DATA_CH1, ((uint16_t) adc_buffer[0]));
	mt->write_word(Register::ADC_DATA_CH2, ((uint16_t) adc_buffer[1]));
	mt->write_word(Register::ADC_DATA_CH3, ((uint16_t) adc_buffer[2]));
	mt->write_word(Register::ADC_DATA_CH4, ((uint16_t) adc_buffer[3]));
}

void Manager::adc_calculate_filter_coefficients()
{
	//	How to calculate the Butterworth low pass filter coefficients:
	//	https://stackoverflow.com/questions/20924868/calculate-coefficients-of-2nd-order-butterworth-low-pass-filter/20932062
}

void Manager::update_encoder_config(uint8_t config, encoder::Encoder* enc)
{
	using namespace memory_table;

	// Reset counting if required
	if ((config & static_cast<uint8_t>(POSITIONS_ENCODER_CONFIG::RESET_COUNTING_MASK)) > 0)
	{
		enc->reset_couting();

		// Also update the memory table
		switch (enc->get_number())
		{
		case ENCODER_1:
			mt->write_dword(Register::ENC_1_COUNT, 0);
			break;

		case ENCODER_2:
			mt->write_dword(Register::ENC_2_COUNT, 0);
			break;

		default:
			break;
		}
	}

	// Reset time measurement if required
	if ((config & static_cast<uint8_t>(POSITIONS_ENCODER_CONFIG::RESET_TIME_MASK)) > 0)
	{
		enc->reset_time_measurement();
	}

	// Set quadrature
	uint8_t new_quadrature = (config & static_cast<uint8_t>(POSITIONS_ENCODER_CONFIG::QUADRATURE_MASK)) >> static_cast<uint8_t>(POSITIONS_ENCODER_CONFIG::QUADRATURE);
	enc->set_quadrature((encoder::Quadrature) new_quadrature);

	// Set direction
	if ((config & static_cast<uint8_t>(POSITIONS_ENCODER_CONFIG::DIRECTION_MASK)) > 0)
		enc->set_cw_is_positive();
	else
		enc->set_ccw_is_positive();

	// Enable or disable the encoder
	if ((config & static_cast<uint8_t>(POSITIONS_ENCODER_CONFIG::ENABLE_MASK)) > 0)
		enc->enable();
	else
		enc->disable();
}

void Manager::update_encoder_readings_in_mt(uint8_t what_encoder)
{
	using namespace memory_table;

	switch (what_encoder) {
	case ENCODER_1:
		mt->write_dword(Register::ENC_1_COUNT, encs[ENCODER_1].get_pulses_count());
		mt->write_dword(Register::ENC_1_TIME, encs[ENCODER_1].get_duration_of_last_pulse());
		break;

	case ENCODER_2:
		mt->write_dword(Register::ENC_2_COUNT, encs[ENCODER_2].get_pulses_count());
		mt->write_dword(Register::ENC_2_TIME, encs[ENCODER_2].get_duration_of_last_pulse());
		break;

	default:
		break;
	}
}





