#include "moving_average.h"

namespace dsp {

MovingAverage::MovingAverage()
{
	index = 0;
	sum = 0;
}

MovingAverage::~MovingAverage()
{
//	delete values;
}

void MovingAverage::begin(uint8_t length)
{
//	values = new uint32_t[length];
	this->length = length;
}

void MovingAverage::add(int32_t value)
{
	sum = sum - values[index];
	values[index] = value;
	sum = sum + values[index];

	index = index + 1;
	if (index == length)
		index = 0;
}

int32_t MovingAverage::get_average()
{
	return sum / length;
}

int32_t MovingAverage::get_sum()
{
	return sum;
}

int32_t* MovingAverage::get_values()
{
	return values;
}

void MovingAverage::reset(int32_t value)
{
	uint8_t i;
	for (i = 0; i < length; i++)
	{
		values[i] = value;
	}

	index = 0;
	sum = length * value;
}

} // namespace dsp
