#ifndef ENCODER_ENCODER_H_
#define ENCODER_ENCODER_H_

/**
  ******************************************************************************
  * @file    encoder.h
  * @author  José Roberto Colombo Junior
  * @brief   Header file of the encoder driver
  ******************************************************************************
  * @attention
  *
  * This is free software licensed under GPL.
  *
  ******************************************************************************
  */

#include <stdint.h>
#include <math.h>
#include "Config/board.h"
#include "main.h"

#include "Drivers/utils/queue.h"
#include "Drivers/dsp/moving_average.h"

/**
 * @defgroup encoder Encoder
 *
 * This is the encoder driver. Only encoders with CHA and CHB are supported.
 *
 * Counting mode supports quadratures 1X, 2X and 4X. The time mode (time between
 * pulses) considers only rising edges from CHA.
 *
 */

/**@{*/

/**
 * Encoder namespace
 */
namespace encoder {

/**
 * @brief Supported quadrature modes
 *
 * This option applies only for counting mode.
 * In order to avoid encoder phase problems, the time measurement occurs
 * only with rising edges of CHA.
 */
enum class Quadrature : uint8_t {
	NONE = 0,											///< No edge is captured (it is used to disable the encoder and it is not available in the computer interface)
	X1 = 1,												///< Capture CHA rising edges
	X2 = 2,												///< Capture CHA rising and falling edges
	X4 = 3,												///< Capture all edges on CHA and CHB
};

/**
 * @brief Encoder gray codes (chB, chA)
 *
 * 4 bit Gray code:
 *
 * | Name             | Decimal | Binary | Gray |
 * | :--------------- | :-----: | :----: | :--: |
 * | GrayCode::GRAY_0 | 0       | 00     | 00   |
 * | GrayCode::GRAY_1 | 1       | 01     | 01   |
 * | GrayCode::GRAY_2 | 2       | 10     | 11   |
 * | GrayCode::GRAY_3 | 3       | 11     | 10   |
 *
 * The herein Gray codes assume the pins as (chB, chA).
 *
 * @note
 * If chB is 1 and chA is 0, then the Gray code is encoder::GrayCode::GRAY_3.
 */
enum class GrayCode : uint8_t {
    GRAY_0 = 0b00,										///< Code 0
    GRAY_1 = 0b01,										///< Code 1
    GRAY_2 = 0b11,										///< Code 2
    GRAY_3 = 0b10,										///< Code 3
};

typedef enum {
    EVENT_NONE = 0,
    EVENT_PIN_CHANGE = 1,
    EVENT_TIMER_OVERFLOW,
} event_type_t;

typedef struct {
    event_type_t type;
    int32_t timer_count_us;
    GrayCode pin_state;
} event_t;

/**
 * @brief This class models an encoder with CHA and CHB.
 */
class Encoder {
private:
    // Pin information
	GPIO_InitTypeDef chA;								///< chA pin handler
    GPIO_TypeDef* chA_pin_port;							///< chA pin port
    IRQn_Type chA_irq_number;							///< chB IRQ number

    GPIO_InitTypeDef chB;								///< chB pin handler
    GPIO_TypeDef* chB_pin_port;							///< chB pin port
    IRQn_Type chB_irq_number;							///< chB IRQ number

    // Configuration
    bool enabled;										///< Is the encoder enabled?
    int8_t cw_increment;								///< The clockwise increment is +1 or -1?
    uint8_t number;										///< Encoder number (example: ENCODER_1, ENCODER_2, etc)
    Quadrature actual_quadrature;						///< Encoder actual quadrature
    int32_t timeout_us = ENC_DEFAULT_TIMEOUT_US;		///< Timeout (maximum measured time length, by default 100ms)
    float max_rate = ENC_DEFAULT_MAX_RATE;				///< Max rate of the input time measurements

    // Encoder state
    GrayCode actual_code;								///< Actual gray state
    int32_t pulses_count;								///< Pulses count
    int32_t duration_of_new_pulse_us;					///< Time that the last pulse occurred
    uint32_t duration_of_last_pulse_us;					///< Duration of the last pulse

    // Filter moving average
    uint8_t filter_size = ENC_DEFAULT_FILTER_SIZE;
    dsp::MovingAverage filter;

    // Implement a ring buffer
    RingBuffer<event_t, ENC_BUFFER_SIZE> queue;

    uint8_t read_gpio_chA();
    uint8_t read_gpio_chB();

    /**
     * This function is used to read the encoder pins
     *
     * @return the encoder actual digital state
     */
    GrayCode read_gpio();

    void enable_gpio_irq();

    void update_gpio_modes(Quadrature new_quadrature);

    void update_state(GrayCode new_code, int32_t timer_value);

    void verify_timeout();
protected:
public:
    Encoder();

    void begin(uint8_t number);

    void update_count(int32_t timer_value_us);

    void update_time(int32_t timer_value_us);

    void update_on_overflow();

    void set_cw_is_positive();

    void set_ccw_is_positive();

    void set_timeout(uint32_t new_timeout);

    uint32_t get_timeout();

    void set_filter_size(uint8_t size);

    uint8_t get_filter_size();

    void set_max_rate(uint8_t new_rate);

    uint8_t get_max_rate();

    uint8_t get_number();

    bool is_enabled();

    void enable();

    void disable();

    void set_quadrature(Quadrature new_quadrature);

    void reset_couting();

    void reset_time_measurement();

    int32_t get_pulses_count();

    uint32_t get_duration_of_last_pulse();

    void isr_pin_change(int32_t timer_count_us);

    void isr_timer_overflow();

    bool callback();
};

}; // namespace encoder

/**@}*/

#endif /* ENCODER_ENCODER_H_ */
