#include "protocol.h"

namespace protocol {

Message::Message()
{
	// Initialize default position of the pointers
	parameters = &data[static_cast<uint8_t>(Field::PARAMETER_FIRST)];

	// Clean message memory
	clean_message();
}

void Message::set_field(Field what_field, uint8_t new_value)
{
	data[static_cast<uint8_t>(what_field)] = new_value;
}

void Message::set_header_1(uint8_t new_value)
{
	set_field(Field::HEADER_1, new_value);
}

void Message::set_header_2(uint8_t new_value)
{
	set_field(Field::HEADER_2, new_value);
}

void Message::set_id(uint8_t new_value)
{
	set_field(Field::ID, new_value);
}

void Message::set_command(uint8_t new_value)
{
	set_field(Field::COMMAND, new_value);
}

void Message::set_length(uint8_t new_value)
{
	set_field(Field::LENGTH, new_value);
	update_checksum_position();
}

void Message::set_checksum(uint8_t new_value)
{
	update_checksum_position();
	*checksum = new_value;
}

uint8_t Message::get_field(Field what_field)
{
	return data[static_cast<uint8_t>(what_field)];
}

uint8_t Message::get_parameter(uint8_t what_parameter)
{
	return parameters[what_parameter];
}

uint8_t* Message::get_addr_of_parameters()
{
	return parameters;
}

uint8_t Message::get_number_of_parameters()
{
	return get_length() - 2;
}

uint8_t Message::get_id()
{
	return get_field(Field::ID);
}

uint8_t Message::get_length()
{
	return get_field(Field::LENGTH);
}

uint8_t Message::get_checksum()
{
	update_checksum_position();
	return *checksum;
}

uint8_t* Message::get_addr_of_packet()
{
	return data;
}

uint8_t Message::get_total_packet_size()
{
	return get_length() + 4;
}

void Message::clean_message()
{
	uint16_t i;
	for (i = 0; i < MSG_SIZE; i++)
		data[i] = 0;
}

void Message::update_checksum_position()
{
	//                         +-------------------------------+
	//                         |       In length = 2 + N       |
	// +----+----+----+--------+-----+--------------+----------+
	// | H1 | H2 | ID | Length | CMD | N parameters | CHECKSUM |
	// +----+----+----+--------+-----+--------------+----------+
	//   0    1    2       3      4       5 + N         6 + N
	checksum = &data[get_field(Field::LENGTH) + 3];
}

uint8_t Message::calculate_checksum()
{
	uint8_t calculated_checksum = 0;
	uint8_t msg_size = get_field(Field::LENGTH) + 4 - 1; // do not count the checksum field

	uint8_t i;
	for (i = static_cast<uint8_t>(Field::ID); i < msg_size; i++)
		calculated_checksum = calculated_checksum + data[i];
	calculated_checksum = (~calculated_checksum) & 0xFF;

	return calculated_checksum;
}

bool Message::is_checksum_valid()
{
	bool is_valid = false;
	uint8_t calculated_checksum = calculate_checksum();

	if (calculated_checksum == get_checksum())
		is_valid = true;

	return is_valid;
}

bool Message::is_for_me()
{
	bool got_my_id = false;
	uint8_t id_msg = get_field(Field::ID);
	if (id_msg == BOARD_ID)
		got_my_id = true;

	return got_my_id;
}

bool Message::is_for_broadcast()
{
	bool got_broadcast_id = false;
	uint8_t id_msg = get_field(Field::ID);
	if (id_msg == BROADCAST_ID)
		got_broadcast_id = true;

	return got_broadcast_id;
}

bool Message::is_valid()
{
	bool is_it = true;

	if (get_field(Field::HEADER_1) != HEADER_1_VALUE)
		is_it = false;

	if (get_field(Field::HEADER_2) != HEADER_2_VALUE)
		is_it = false;

	if (!is_checksum_valid())
		is_it = false;

	return is_it;
}

void Message::append_parameter(uint8_t value)
{
	uint8_t actual_length = get_field(Field::LENGTH);
	parameters[actual_length - 2] = value;
	set_field(Field::LENGTH, actual_length + 1);
}

void Message::insert_parameter(uint8_t index, uint8_t value)
{
	parameters[index] = value;
}

void Message::update_checksum_value()
{
	uint8_t checksum = calculate_checksum();
	set_checksum(checksum);
}

InstructionPacket::InstructionPacket()
{
	Message();
}

bool InstructionPacket::is_this_command_valid(uint8_t command)
{
	bool is_valid;
	switch (command)
	{
		case static_cast<uint8_t>(Command::PING):
			is_valid = true;
			break;

		case static_cast<uint8_t>(Command::READ):
			is_valid = true;
			break;

		case static_cast<uint8_t>(Command::WRITE):
			is_valid = true;
			break;

		case static_cast<uint8_t>(Command::REBOOT):
			is_valid = true;
			break;

		default:
			is_valid = false;
			break;
	}

	return is_valid;
}

bool InstructionPacket::is_command_valid()
{
	return is_this_command_valid(get_field(Field::COMMAND));
}

bool InstructionPacket::is_valid()
{
	bool is_it = true;

	if (get_field(Field::HEADER_1) != HEADER_1_VALUE)
		is_it = false;

	if (get_field(Field::HEADER_2) != HEADER_2_VALUE)
		is_it = false;

	if (!is_command_valid())
		is_it = false;

	if (!is_checksum_valid())
		is_it = false;

	return is_it;
}

Command InstructionPacket::get_command()
{
	return (Command) get_field(Field::COMMAND);
}

StatusPacket::StatusPacket()
{
//	Message();
	// Initialize default position of the pointers
	parameters = &data[static_cast<uint8_t>(Field::PARAMETER_FIRST)];

	// Clean message memory
	clean_message();
}

void StatusPacket::create_new_msg()
{
	set_field(Field::HEADER_1, HEADER_1_VALUE);
	set_field(Field::HEADER_2, HEADER_2_VALUE);
	set_field(Field::ID, BOARD_ID);
	set_field(Field::LENGTH, 2); // CMD and CHKSUM are always sent
	set_field(Field::ERROR, 0);
}

void StatusPacket::append_error(Error what_error)
{
	uint8_t actual_errors = get_field(Field::ERROR);
	set_field(Field::ERROR, actual_errors + static_cast<uint8_t>(what_error));
}

void StatusPacket::clear_error(Error what_error)
{
	uint8_t actual_errors = get_field(Field::ERROR);
	uint8_t error_to_clear = static_cast<uint8_t>(what_error);
	set_field(Field::ERROR, actual_errors & (~error_to_clear));
}

void StatusPacket::clear_errors()
{
	set_field(Field::ERROR, 0);
}

bool StatusPacket::is_valid()
{
	bool is_it = true;

	if (get_field(Field::HEADER_1) != HEADER_1_VALUE)
		is_it = false;

	if (get_field(Field::HEADER_2) != HEADER_2_VALUE)
		is_it = false;

	if (!is_checksum_valid())
		is_it = false;

	return is_it;
}

}



