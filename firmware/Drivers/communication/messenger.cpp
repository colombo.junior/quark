#include "messenger.h"
#include "Config/board.h"

namespace messenger {

Messenger::Messenger()
{
	new_message = false;
	is_valid = false;
	errors = 0;
}

void Messenger::isr_received_new_byte(uint8_t new_byte)
{
	// Check timeout
	if (state_receiving_msg != StateReceivingMsg::LOOKING_FOR_HEADER_1)
	{
		uint32_t ellapsed_time = HAL_GetTick() - time_received_header_1;
		if (ellapsed_time > timeout_max)
		{
			state_receiving_msg = StateReceivingMsg::LOOKING_FOR_HEADER_1;
			n_parameters_received = 0;
		}
	}

	switch (state_receiving_msg) {
	    case StateReceivingMsg::LOOKING_FOR_HEADER_1:
	        if (new_byte == protocol::HEADER_1_VALUE)
	        {
	        	state_receiving_msg = StateReceivingMsg::LOOKING_FOR_HEADER_2;
	        	instruction_packet.set_header_1(new_byte);
	        	time_received_header_1 = HAL_GetTick();
	        }
	        break;

	    case StateReceivingMsg::LOOKING_FOR_HEADER_2:
	    	if (new_byte == protocol::HEADER_2_VALUE)
	    	{
	    		state_receiving_msg = StateReceivingMsg::LOOKING_FOR_ID;
	        	instruction_packet.set_header_2(new_byte);
	    	}
	    	else
	    	{
	    		state_receiving_msg = StateReceivingMsg::LOOKING_FOR_HEADER_1;
	    	}
	    	break;

	    case StateReceivingMsg::LOOKING_FOR_ID:
	    	instruction_packet.set_id(new_byte);
	    	state_receiving_msg = StateReceivingMsg::LOOKING_FOR_LENGTH;
	    	break;

	    case StateReceivingMsg::LOOKING_FOR_LENGTH:
	    	instruction_packet.set_length(new_byte);
	    	n_parameters_received = 0;
	    	state_receiving_msg = StateReceivingMsg::LOOKING_FOR_CMD;
	    	break;

	    case StateReceivingMsg::LOOKING_FOR_CMD:
	    	instruction_packet.set_command(new_byte);
	    	state_receiving_msg = StateReceivingMsg::RECEIVING_MSG;
	    	break;

	    case StateReceivingMsg::RECEIVING_MSG:
	    	instruction_packet.insert_parameter(n_parameters_received, new_byte);
	    	n_parameters_received = n_parameters_received + 1;

	    	if ((n_parameters_received + 2) == instruction_packet.get_length())
	    	{
	    		state_receiving_msg = StateReceivingMsg::LOOKING_FOR_CHECKSUM;
	    	}
	    	break;

	    case StateReceivingMsg::LOOKING_FOR_CHECKSUM:
	    	// Copy the received byte to the checksum field
	    	instruction_packet.set_checksum(new_byte);

	    	if (instruction_packet.is_for_me() || instruction_packet.is_for_broadcast())
	    	{
				new_message = true;

				is_valid = true;
				if (instruction_packet.is_command_valid() == false)
				{
					errors = errors + static_cast<uint8_t>(protocol::Error::INVALID_INSTRUCTION);
					is_valid = false;
				}

				if (instruction_packet.is_checksum_valid() == false)
				{
					errors = errors + static_cast<uint8_t>(protocol::Error::INVALID_CHECKSUM);
					is_valid = false;
				}
	    	}

	    	state_receiving_msg = StateReceivingMsg::LOOKING_FOR_HEADER_1;
	    	break;
	}
}

void Messenger::instruction_packet_was_processed()
{
	is_valid = false;
	new_message = false;
	errors = 0;
}

void Messenger::send_packet(protocol::Message* packet)
{
	uint8_t* addr = packet->get_addr_of_packet();
	uint16_t size = packet->get_total_packet_size();
	CDC_Transmit_FS(addr, size);
}

}
