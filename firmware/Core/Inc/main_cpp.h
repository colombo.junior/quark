#ifndef INC_MAIN_CPP_H_
#define INC_MAIN_CPP_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

extern void main_cpp();
extern void ISR_USB(uint8_t* buffer, uint32_t length);

#ifdef __cplusplus
}
#endif



#endif /* INC_MAIN_CPP_H_ */
