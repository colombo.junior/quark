#ifndef INC_ISR_FUNCTIONS_H_
#define INC_ISR_FUNCTIONS_H_

#include "main.h"
#include "stm32f1xx_it.h"

#ifdef __cplusplus
	extern "C" {
#endif

	// EXTI0_IRQHandler
	// EXTI1_IRQHandler
	// EXTI2_IRQHandler
	// EXTI3_IRQHandler
	// EXTI4_IRQHandler
	// EXTI9_5_IRQHandler
	// EXTI15_10_IRQHandler

	void EXTI4_IRQHandler(void);

	void EXTI9_5_IRQHandler(void);

	void EXTI15_10_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif /* INC_ISR_FUNCTIONS_H_ */
