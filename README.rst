Quark
=======

Quark is a firmware for STM32 micro-controllers to make them operate as data 
acquisition boards. It was designed to be easy to use for control applications:

- It is controlled by the computer through a USB 2.0 cable
- In theory, rates of until 4 kHz can be obtained
- The following hardware is supported:

    - Read up to 2 encoders (count and pulse duration measurements)
    - 4x A/D of 12 bits, reading voltages from 0 to 3.3 V
    - 4x PWM channels and it is possible to set the PWM frequency
    - 4x GPIO output (push-pull and open-drain modes)
    - 4x GPIO input
    - 1x I2C (drivers for devices coming soon)
    - 3x OneWire (drivers for devices coming soon)

The firmware is written in C++ but you do not need to worry. The board can be 
controlled from:

- Python 3 (pure Python implementation)
- MATLAB/Simulink (support coming soon)
- Any language/computer that supports reading/writing UART ports 


Example of Python interface
-----------------------------

>>> from quark import *
>>> board = Quark('COMx')
>>> board.adc_enable()
>>> board.adc_read(CH1)
>>> board.pwm_write(CH1, 0.15)  # write 15% duty cycle to CH1


Documentation access
----------------------

Click `here <https://colombo.junior.gitlab.io/quark/>`_.


Ok, shutup and take my money
------------------------------

What do I need to do/buy to use it? For now, you need to buy a BluePill
development board (around 2 US) and a USB cable.


Very important!
-----------------

This firmware (and its interfaces) are developed only for educational 
purposes. **It is not intended to be a commercial product in any way**.
