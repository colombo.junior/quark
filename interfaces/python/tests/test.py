# import sys
# sys.path.append('/home/junior/repo/quark/cmake-build-debug/interfaces/python')

import time
try:
    from quark import *
except:
    from interfaces.python.quark import *

board = Quark('/dev/ttyACM0')
# board.set_gpio_output_mode(CH3, OPEN_DRAIN)
# board.set_gpio_output_pull(CH3, PULL_UP)

for i in range(0, 10):
    ti = time.time()
    board.digital_write(CH3, LOW)
    tf = time.time()

    print('Tempo: %f us' % (1e6 * (tf - ti)))

    time.sleep(0.1)
    board.digital_write(CH3, HIGH)
    time.sleep(0.1)

