#include <pybind11/pybind11.h>
#include "interfaces/cpp/quark.h"

namespace py = pybind11;

PYBIND11_MODULE(quark_python, m) {
    m.attr("CH1") = CH1;
    m.attr("CH2") = CH2;
    m.attr("CH3") = CH3;
    m.attr("CH4") = CH4;
    m.attr("HIGH") = HIGH;
    m.attr("LOW") = LOW;
    m.attr("PULL_NONE") = (int) PULL_NONE;
    m.attr("PULL_DOWN") = (int) PULL_DOWN;
    m.attr("PULL_UP") = (int) PULL_UP;
    m.attr("OPEN_DRAIN") = (int) OPEN_DRAIN;
    m.attr("PUSH_PULL") = (int) PUSH_PULL;

    m.attr("ENCODER_CW_IS_POSITIVE") = ENCODER_CW_IS_POSITIVE;
    m.attr("ENCODER_CCW_IS_POSITIVE") = ENCODER_CCW_IS_POSITIVE;

    m.attr("ENCODER_QUADRATURE_X1") = ENCODER_QUADRATURE_1X;
    m.attr("ENCODER_QUADRATURE_X2") = ENCODER_QUADRATURE_2X;
    m.attr("ENCODER_QUADRATURE_X4") = ENCODER_QUADRATURE_4X;

    py::class_<Quark> quark(m, "QuarkCpp");

    quark.def(py::init<char*>());

    quark.def(
            "read_all_cpp",
            &Quark::read_all
    );

    quark.def(
            "set_gpio_input_pull_cpp",
            &Quark::set_gpio_input_pull
    );

    quark.def(
            "set_gpio_output_pull_cpp",
            &Quark::set_gpio_output_pull
    );

    quark.def(
            "set_gpio_output_mode_cpp",
            &Quark::set_gpio_output_mode
    );

    quark.def(
            "digital_read_cpp",
            &Quark::digital_read
    );

    quark.def(
            "digital_write_cpp",
            &Quark::digital_write
    );

    quark.def(
            "analog_read_cpp",
            &Quark::analog_read
    );

    quark.def(
            "adc_enable_cpp",
            &Quark::adc_enable
    );

    quark.def(
            "adc_disable_cpp",
            &Quark::adc_disable
    );

    quark.def(
            "pwm_write_cpp",
            &Quark::pwm_write
    );

    quark.def(
            "pwm_set_frequency_cpp",
            &Quark::pwm_set_frequency
    );

    quark.def(
            "pwm_get_frequency_cpp",
            &Quark::pwm_get_frequency
    );

    quark.def(
            "encoder_1_read_count_cpp",
            &Quark::encoder_1_read_count
    );

    quark.def(
            "encoder_2_read_count_cpp",
            &Quark::encoder_2_read_count
    );

    quark.def(
            "encoder_1_read_time_cpp",
            &Quark::encoder_1_read_time
    );

    quark.def(
            "encoder_2_read_time_cpp",
            &Quark::encoder_2_read_time
    );

    quark.def(
            "encoder_1_set_direction_cpp",
            &Quark::encoder_1_set_direction
    );

    quark.def(
            "encoder_2_set_direction_cpp",
            &Quark::encoder_2_set_direction
    );

    quark.def(
            "encoder_1_reset_counting_cpp",
            &Quark::encoder_1_reset_counting
    );

    quark.def(
            "encoder_2_reset_counting_cpp",
            &Quark::encoder_2_reset_counting
    );

    quark.def(
            "encoder_1_enable_cpp",
            &Quark::encoder_1_enable
    );

    quark.def(
            "encoder_2_enable_cpp",
            &Quark::encoder_2_enable
    );

    quark.def(
            "encoder_1_disable_cpp",
            &Quark::encoder_1_disable
    );

    quark.def(
            "encoder_2_disable_cpp",
            &Quark::encoder_2_disable
    );

    quark.def(
            "encoder_1_set_quadrature_cpp",
            &Quark::encoder_1_set_quadrature
    );

    quark.def(
            "encoder_2_set_quadrature_cpp",
            &Quark::encoder_2_set_quadrature
    );

    quark.def(
            "encoder_1_set_timeout_cpp",
            &Quark::encoder_1_set_timeout
    );

    quark.def(
            "encoder_2_set_timeout_cpp",
            &Quark::encoder_2_set_timeout
    );

    quark.def(
            "encoder_1_set_filter_size_cpp",
            &Quark::encoder_1_set_filter_size
    );

    quark.def(
            "encoder_2_set_filter_size_cpp",
            &Quark::encoder_2_set_filter_size
    );

    quark.def(
            "encoder_1_set_rel_max_rate_cpp",
            &Quark::encoder_1_set_rel_max_rate
    );

    quark.def(
            "encoder_2_set_rel_max_rate_cpp",
            &Quark::encoder_2_set_rel_max_rate
    );

    quark.def(
            "get_firmware_major_version_cpp",
            &Quark::get_firmware_major_version
    );

    quark.def(
            "get_firmware_minor_version_cpp",
            &Quark::get_firmware_minor_version
    );

    quark.def(
            "get_firmware_patch_version_cpp",
            &Quark::get_firmware_patch_version
    );

    quark.def(
            "get_who_am_i_cpp",
            &Quark::get_who_am_i
    );

    quark.def(
            "close_cpp",
            &Quark::close
    );
}



