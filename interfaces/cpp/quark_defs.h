#ifndef QUARK_QUARK_DEFS_H
#define QUARK_QUARK_DEFS_H

constexpr u8 QUARK_ID = 0x40;

constexpr int HIGH = 1;
constexpr int LOW = 0;

constexpr int CH1 = 1;
constexpr int CH2 = 2;
constexpr int CH3 = 3;
constexpr int CH4 = 4;

constexpr double K_ADC = 3.3 / 4095;

constexpr float PWM_MIN_FREQUENCY =				0.02;
constexpr float PWM_MAX_FREQUENCY =				720000;
constexpr float PWM_MIN_DUTY =					0;
constexpr float PWM_MAX_DUTY =					1;

/**
 * @brief Possible configurations of the GPIO pull-resistors
 */
constexpr int PULL_NONE = 0b00;                      ///< No pull resistor
constexpr int PULL_UP = 0b01;                        ///< Use a pull-up resistor
constexpr int PULL_DOWN = 0b10;                      ///< Use a pull-down resistor

/**
 * @brief Available output modes
 */
constexpr int PUSH_PULL = 0;                         ///< Push-pull (forces LOW or HIGH values)
constexpr int OPEN_DRAIN = 1;                        ///< Open-drain (forces only LOW values, HIGH is achieved by the use of a pull-up resistor)

/**
 * @brief Positions of the Register::GPIO_INPUT_PULL
 */
enum class eGPIO_INPUT_PULL : uint8_t {
    CH1 = 0,                                        ///< CH1 position
    CH2 = 2,                                        ///< CH2 position
    CH3 = 4,                                        ///< CH3 position
    CH4 = 6,                                        ///< CH4 position
    CH1_MASK = 0b11u << CH1,                        ///< CH1 mask
    CH2_MASK = 0b11u << CH2,                        ///< CH2 mask
    CH3_MASK = 0b11u << CH3,                        ///< CH3 mask
    CH4_MASK = 0b11u << CH4,                        ///< CH4 mask
};

/**
 * @brief Positions of the Register::GPIO_INPUT_STATE
 */
enum class eGPIO_INPUT_STATE : uint8_t {
    CH1 = 0,                                        ///< CH1 position
    CH2 = 1,                                        ///< CH2 position
    CH3 = 2,                                        ///< CH3 position
    CH4 = 3,                                        ///< CH4 position
    CH1_MASK = (1u << CH1),                         ///< CH1 mask
    CH2_MASK = (1u << CH2),                         ///< CH2 mask
    CH3_MASK = (1u << CH3),                         ///< CH3 mask
    CH4_MASK = (1u << CH4),                         ///< CH4 mask
};

/**
 * @brief Positions of the Register::GPIO_OUTPUT_STATE
 */
enum class eGPIO_OUTPUT_STATE : uint8_t {
    CH1 = 0,                                        ///< CH1 position
    CH2 = 1,                                        ///< CH2 position
    CH3 = 2,                                        ///< CH3 position
    CH4 = 3,                                        ///< CH4 position
    CH1_MASK = (1u << CH1),                         ///< CH1 mask
    CH2_MASK = (1u << CH2),                         ///< CH2 mask
    CH3_MASK = (1u << CH3),                         ///< CH3 mask
    CH4_MASK = (1u << CH4),                         ///< CH4 mask
};

/**
 * @brief Positions of the Register::GPIO_OUTPUT_PULL
 */
enum class eGPIO_OUTPUT_PULL : uint8_t {
    CH1 = 0,                                        ///< CH1 position
    CH2 = 2,                                        ///< CH2 position
    CH3 = 4,                                        ///< CH3 position
    CH4 = 6,                                        ///< CH4 position
    CH1_MASK = 0b11u << CH1,                        ///< CH1 mask
    CH2_MASK = 0b11u << CH2,                        ///< CH2 mask
    CH3_MASK = 0b11u << CH3,                        ///< CH3 mask
    CH4_MASK = 0b11u << CH4,                        ///< CH4 mask
};

/**
 * @brief Positions of the Register::GPIO_OUTPUT_MODE
 */
enum class eGPIO_OUTPUT_MODE : uint8_t {
    CH1 = 0,                                        ///< CH1 position
    CH2 = 1,                                        ///< CH2 position
    CH3 = 2,                                        ///< CH3 position
    CH4 = 3,                                        ///< CH4 position
    CH1_MASK = (1u << CH1),                         ///< CH1 mask
    CH2_MASK = (1u << CH2),                         ///< CH2 mask
    CH3_MASK = (1u << CH3),                         ///< CH3 mask
    CH4_MASK = (1u << CH4),                         ///< CH4 mask
};

/**
 * Positions of the Register::ADC_CONFIG
 */
enum class eADC_CONFIG : uint8_t {
    ENABLE = 0,                                     ///< Enable position
    ENABLE_MASK = (1 << ENABLE),                    ///< Enable mask
};

/**
 * Positions of the Registers ENCODER_1_CONFIG
 */
enum class eENCODER_CONFIG : uint8_t {
    QUADRATURE = 0,                                 ///< Quadrature bits
    DIRECTION = 2,                                  ///< Used to set if counts +1 in CW or CCW rotation
    RESET_COUNTING = 3,                             ///< Reset the pulses count
    ENABLE = 4,                                     ///< Enable or disable
    RESET_TIME = 5,                                 ///< Reset the time measurement
    QUADRATURE_MASK = (0b11 << QUADRATURE),         ///< Quadrature mask
    DIRECTION_MASK = (1 << DIRECTION),              ///< Direction mask
    RESET_COUNTING_MASK = (1 << RESET_COUNTING),    ///< Reset counting mask
    ENABLE_MASK = (1 << ENABLE),                    ///< Enable mask
    RESET_TIME_MASK = (1 << RESET_TIME),            ///< Time measurement reset mask
};

constexpr int ENCODER_QUADRATURE_1X = 1;
constexpr int ENCODER_QUADRATURE_2X = 2;
constexpr int ENCODER_QUADRATURE_4X = 3;

constexpr int ENCODER_CW_IS_POSITIVE = 1;
constexpr int ENCODER_CCW_IS_POSITIVE = 0;

#endif //QUARK_QUARK_DEFS_H
