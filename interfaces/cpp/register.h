#ifndef SJBOTS_VSS_REGISTER_H
#define SJBOTS_VSS_REGISTER_H

#include "utils/types.h"

class AbstractRegister {
public:
    virtual ~AbstractRegister(){}

    virtual u16 get_addr() {
        return address;
    }

protected:
    const u16 address = 0;
    const u8* memory = nullptr;
    AbstractRegister(u16 addr, u8* ptr_to_memory) :
            address(addr), memory(ptr_to_memory) {}
};


template <typename T>
class Register: public AbstractRegister {
protected:
    T* value;

public:
    Register(u16 addr, const T initial_value, u8* ptr_to_memory) :
            AbstractRegister(addr, ptr_to_memory)
    {
        value = (T*) &memory[address];
        *value = initial_value;
    }

    T read()
    {
        return *value;
    }

    u8* get_pointer()
    {
        return (u8*) value;
    }

    void write(T new_value)
    {
        *value = new_value;
    }

    void write(u8* data)
    {
        T* ptr = (T*) data;
        *value = *ptr;
    }

    void set_bit(T what_bit)
    {
        write(*value | (1 << what_bit));
    }

    void clear_bit(T what_bit)
    {
        write(*value & ~(1 << what_bit));
    }

    u16 get_addr()
    {
        return address;
    }

    void operator=(const T new_value)
    {
        write(new_value);
    }

    T operator()()
    {
        return read();
    }

    void operator|=(T new_mask)
    {
        write(*value | new_mask);
    }

    void operator&=(T new_mask)
    {
        write(*value & new_mask);
    }

    u16 get_total_size()
    {
        return sizeof(T);
    }
};


template <typename T>
class ArrayRegister: public AbstractRegister {
protected:
    T* value;
    u8 number_of_elements;

public:
    ArrayRegister(u16 addr, const u16 length, u8* ptr_to_memory) :
            AbstractRegister(addr, ptr_to_memory)
    {
        value = (T*) &memory[address];
        number_of_elements = length;
    }

    ~ArrayRegister()
    {
    }

    T read(u16 position)
    {
        return value[position];
    }

    T* get_pointer()
    {
        return value;
    }

    void write(u16 position, T new_value)
    {
        value[position] = new_value;
    }

    void write(u8* data)
    {
        u8 i;
        T* ptr = (T*) data;
        for (i = 0; i < number_of_elements; i++)
            value[i] = ptr[i];
    }

    u16 get_addr(u16 position)
    {
        return address + position;
    }

    u8 get_number_of_elements()
    {
        return number_of_elements;
    }

    u16 get_total_size()
    {
        return number_of_elements * sizeof(T);
    }
};

#endif //SJBOTS_VSS_REGISTER_H
