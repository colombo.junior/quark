#include "dxl_protocol.h"

namespace dxl {

    Message::Message(u16 memory_size): data_size(memory_size)
    {
        data = new u8[memory_size];

        // Initialize default position of the pointers
        header_1 = &data[static_cast<u8>(Field::HEADER_1)];
        header_2 = &data[static_cast<u8>(Field::HEADER_2)];
        id = &data[static_cast<u8>(Field::ID)];
        length = &data[static_cast<u8>(Field::LENGTH)];
        command = &data[static_cast<u8>(Field::COMMAND_OR_ERROR)];
        error = &data[static_cast<u8>(Field::COMMAND_OR_ERROR)];
        parameters = &data[static_cast<u8>(Field::FIRST_PARAMETER)];

        // Clean message memory
        clean_message();
    }

    Message::~Message()
    {
    }

    void Message::set_header_1(u8 new_value)
    {
        *header_1 = new_value;
    }

    void Message::set_header_2(u8 new_value)
    {
        *header_2 = new_value;
    }

    void Message::set_id(u8 new_value)
    {
        *id = new_value;
    }

    void Message::set_command(u8 new_value)
    {
        *command = new_value;
    }

    void Message::set_length(u8 new_value)
    {
        *length = new_value;
        update_checksum_position();
    }

    void Message::set_checksum(u8 new_value)
    {
        update_checksum_position();
        *checksum = new_value;
    }

    u8 Message::get_parameter(u16 what_parameter)
    {
        return parameters[what_parameter];
    }

    u8* Message::get_addr_of_parameters()
    {
        return parameters;
    }

    u8 Message::get_number_of_parameters()
    {
        return get_length() - 2;
    }

    u8 Message::get_id()
    {
        return *id;
    }

    u8 Message::get_length()
    {
        return *length;
    }

    u8 Message::get_checksum()
    {
        update_checksum_position();
        return *checksum;
    }

    u8* Message::get_addr_of_packet()
    {
        return data;
    }

    u16 Message::get_total_packet_size()
    {
        return *length + 4;
    }

    void Message::clean_message()
    {
        u16 i;
        for (i = 0; i < data_size; i++)
            data[i] = 0;
    }

    void Message::update_checksum_position()
    {
        //                         +-------------------------------+
        //                         |       In length = 2 + N       |
        // +----+----+----+--------+-----+--------------+----------+
        // | H1 | H2 | ID | Length | CMD | N parameters | CHECKSUM |
        // +----+----+----+--------+-----+--------------+----------+
        //   0    1    2       3      4       5 + N         6 + N
        checksum = &data[*length + 3];
    }

    u8 Message::calculate_checksum()
    {
        u8 calculated_checksum = 0;
        u8 msg_size = *length + 4 - 1; // do not count the checksum field

        u8 i;
        for (i = static_cast<u8>(Field::ID); i < msg_size; i++)
            calculated_checksum = calculated_checksum + data[i];
        calculated_checksum = (~calculated_checksum) & 0xFF;

        return calculated_checksum;
    }

    bool Message::is_checksum_valid()
    {
        bool is_valid = false;
        u8 calculated_checksum = calculate_checksum();

        if (calculated_checksum == get_checksum())
            is_valid = true;

        return is_valid;
    }

    bool Message::is_for_me(u8 my_id)
    {
        bool got_my_id = false;
        if (*id == my_id)
            got_my_id = true;

        return got_my_id;
    }

    bool Message::is_for_broadcast()
    {
        bool got_broadcast_id = false;
        if (*id == BROADCAST_ID)
            got_broadcast_id = true;

        return got_broadcast_id;
    }

    bool Message::is_valid()
    {
        bool is_it = true;

        if (*header_1 != HEADER_1_VALUE)
            is_it = false;

        if (*header_2 != HEADER_2_VALUE)
            is_it = false;

        if (!is_checksum_valid())
            is_it = false;

        return is_it;
    }

    void Message::append_parameter(u8 value)
    {
        parameters[*length - 2] = value;
        *length = *length + 1;
    }

    void Message::append_parameters(u8* values, u8 size)
    {
        u8 i;
        for (i = 0; i < size; i++)
            parameters[*length - 2 + i] = values[i];

        *length = *length + size;
    }

    void Message::insert_parameter(u16 index, u8 value)
    {
        parameters[index] = value;
    }

    void Message::update_checksum_value()
    {
        u8 checksum = calculate_checksum();
        set_checksum(checksum);
    }

    InstructionPacket::InstructionPacket(u16 memory_size) : Message(memory_size)
    {
    }

    InstructionPacket::~InstructionPacket()
    {
        delete data;
    }

    bool InstructionPacket::is_this_command_valid(u8 command)
    {
        bool is_valid;
        switch (command)
        {
            case static_cast<u8>(Command::PING):
                is_valid = true;
                break;

            case static_cast<u8>(Command::READ):
                is_valid = true;
                break;

            case static_cast<u8>(Command::WRITE):
                is_valid = true;
                break;

            case static_cast<u8>(Command::REBOOT):
                is_valid = true;
                break;

            case static_cast<u8>(Command::ACTION):
                is_valid = true;
                break;

            case static_cast<u8>(Command::BULK_READ):
                is_valid = true;
                break;

            case static_cast<u8>(Command::FACTORY_RESET):
                is_valid = true;
                break;

            case static_cast<u8>(Command::REG_WRITE):
                is_valid = true;
                break;

            case static_cast<u8>(Command::SYNC_WRITE):
                is_valid = true;
                break;

            default:
                is_valid = false;
                break;
        }

        return is_valid;
    }

    bool InstructionPacket::is_command_valid()
    {
        return is_this_command_valid(*command);
    }

    void InstructionPacket::create_new_msg(u8 to_id, Command cmd)
    {
        *header_1 = HEADER_1_VALUE;
        *header_2 = HEADER_2_VALUE;
        *id = to_id;
        *length = 2;
        *command = static_cast<u8>(cmd);
    }

    bool InstructionPacket::is_valid()
    {
        bool is_it = true;

        if (*header_1 != HEADER_1_VALUE)
            is_it = false;

        if (*header_2 != HEADER_2_VALUE)
            is_it = false;

        if (!is_command_valid())
            is_it = false;

        if (!is_checksum_valid())
            is_it = false;

        return is_it;
    }

    Command InstructionPacket::get_command()
    {
        return (Command) *command;
    }

    StatusPacket::StatusPacket(u16 memory_size) : Message(memory_size)
    {
    }

    StatusPacket::~StatusPacket()
    {
        delete data;
    }

    void StatusPacket::create_new_msg(u8 from_id)
    {
        *header_1 = HEADER_1_VALUE;
        *header_2 = HEADER_2_VALUE;
        *id = from_id;
        *length = 2;
        *error = 0;
    }

    void StatusPacket::append_error(Error what_error)
    {
        *error = *error + static_cast<u8>(what_error);
    }

    void StatusPacket::clear_error(Error what_error)
    {
        *error = *error & (~static_cast<u8>(what_error));
    }

    void StatusPacket::clear_errors()
    {
        *error = 0;
    }

    bool StatusPacket::is_valid()
    {
        bool is_it = true;

        if (*header_1 != HEADER_1_VALUE)
            is_it = false;

        if (*header_2 != HEADER_2_VALUE)
            is_it = false;

        if (!is_checksum_valid())
            is_it = false;

        return is_it;
    }

    Receiver::Receiver(Message* msg_ptr)
    {
        msg = msg_ptr;
        state = State::LOOKING_FOR_HEADER_1;
        clean();
    }

    Receiver::~Receiver()
    {
    }

    void Receiver::process_new_byte(u8 new_byte)
    {
        // Check timeout
        if (state != State::LOOKING_FOR_HEADER_1)
        {
            u32 elapsed_time = get_time_us() - time_received_header_1;
            if (elapsed_time > timeout_us)
            {
                state = State::LOOKING_FOR_HEADER_1;
                n_parameters_to_receive = 0;
            }
        }

        switch (state)
        {
        case State::LOOKING_FOR_HEADER_1:
            if (new_byte == HEADER_1_VALUE)
            {
                state = State::LOOKING_FOR_HEADER_2;
                msg->set_header_1(new_byte);
                time_received_header_1 = get_time_us();
            }
            break;

        case State::LOOKING_FOR_HEADER_2:
            if (new_byte == HEADER_2_VALUE)
            {
                state = State::LOOKING_FOR_ID;
                msg->set_header_2(new_byte);
            }

            else
            {
                state = State::LOOKING_FOR_HEADER_1;
            }
            break;

        case State::LOOKING_FOR_ID:
            msg->set_id(new_byte);
            state = State::LOOKING_FOR_LENGTH;
            break;

        case State::LOOKING_FOR_LENGTH:
            msg->set_length(new_byte);
                n_parameters_to_receive = msg->get_length();
            state = State::LOOKING_FOR_CMD_OR_ERROR;
            break;

        case State::LOOKING_FOR_CMD_OR_ERROR:
            msg->set_command(new_byte);
                n_parameters_to_receive = n_parameters_to_receive - 1;

            if (n_parameters_to_receive == 1)
                state = State::LOOKING_FOR_CHECKSUM;
            else
                state = State::RECEIVING_PARAMETERS;
            break;

        case State::RECEIVING_PARAMETERS:
//            msg->append_parameter(new_byte);
            msg->insert_parameter(msg->get_length() - n_parameters_to_receive - 1, new_byte);
                n_parameters_to_receive = n_parameters_to_receive - 1;

            // It remains to receive the checksum only
            if (n_parameters_to_receive == 1)
            {
                state = State::LOOKING_FOR_CHECKSUM;
            }

            break;

        case State::LOOKING_FOR_CHECKSUM:
            // Copy the received byte to the checksum field
            msg->set_checksum(new_byte);
                n_parameters_to_receive = n_parameters_to_receive - 1;

            // Notify that a new message arrived
            new_message = true;

            // Verify if message is valid
            is_valid = msg->is_valid();

            // Restart the message search
            state = State::LOOKING_FOR_HEADER_1;
            break;
        }
    }

    void Receiver::received_new_data(u8* data, u16 size)
    {
        u16 i = 0;
        for (i = 0; i < size; i++)
        {
            process_new_byte(data[i]);
        }
    }

    void Receiver::clean()
    {
        msg->clean_message();
        new_message = false;
        is_valid = false;
        errors = 0;
    }

} // dxl_protocol
