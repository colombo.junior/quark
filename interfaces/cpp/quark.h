#ifndef QUARK_QUARK_H
#define QUARK_QUARK_H

#define QUARK_DEBUG_LEVEL 0

#include <stdio.h>
#include <QtSerialPort/QSerialPort>

#include "utils/time.h"
#include "dxl_protocol.h"
#include "register.h"
#include "quark_defs.h"

/**
 * Quark firmware minimum version.
 */
constexpr int MIN_VERSION_MAJOR = 1;
constexpr int MIN_VERSION_MINOR = 0;
constexpr int MIN_VERSION_PATCH = 0;

constexpr int MEMORY_TABLE_SIZE = 128;

class Quark {
public:
    /**
     * Constructor. No parameter is required.
     */
    Quark();

    /**
     * Constructor. No parameter is required.
     */
    Quark(char* port_name);

    /**
     * Destructor.
     */
    ~Quark();

    /**
     * The begin method must be used to initialize the Quark object.
     *
     * @param serial_port_handler is a Qt serial port handler.
     */
    void begin(char* port_name);

    /**
     * It reads all the memory of the board. If high sample rates are desired,
     * then it is recommended to call this method in the begining of the loop.
     * So it will acquire all the board data at once.
     */
    void read_all();

    /**
     * This method sets the input pull resistors of an input digital pin.
     *
     * @param channel the INPUT GPIO channel. Example: CH1.
     * @param pull the pull resistor. Example: eGPIO_PULL::UP.
     */
    void set_gpio_input_pull(int channel, int pull);


    /**
     * This method sets the output pull resistors of an output digital pin.
     *
     * @param channel the OUTPUT GPIO channel. Example: CH1.
     * @param pull the pull resistor. Example: eGPIO_PULL::UP.
     */
    void set_gpio_output_pull(int channel, int pull);

    /**
     * This method sets the driver mode of an output digital pin.
     *
     * @param channel the OUTPUT GPIO channel. Example: CH1.
     * @param mode the driver mode. Example: eOUTPUT_MODE::PUSH_PULL.
     */
    void set_gpio_output_mode(int channel, int mode);

    /**
     * This method reads the logic state of a digital input pin.
     *
     * @param channel the INPUT GPIO channel. Example: CH1.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return HIGH or LOW.
     */
    int digital_read(int channel, bool from_buffer=false);

    /**
     * This method writes HIGH or LOW value to a output digital pin.
     *
     * @param channel the OUTPUT GPIO channel. Example: CH3.
     * @param state HIGH or LOW.
     */
    void digital_write(int channel, int state);

    /**
     * This method reads the analog value on an analog channel.
     * @param channel the ANALOG channel. Example: CH3.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return the voltage (in V) on the analog pin.
     */
    double analog_read(int channel, bool from_buffer=false);

    /**
     * This method enables the ADC.
     */
    void adc_enable();

    /**
     * This method disables the ADC.
     */
    void adc_disable();

    /**
     * This method sets the duty cycle of some PWM channel.
     * @param channel the PWM channel. Example: CH3.
     * @param duty a real number between 0 and 1.
     */
    void pwm_write(int channel, double duty);

    /**
     * This method sets the PWM frequency.
     * @param freq_in_hz the frequency (in Hz) of the PWM signal.
     */
    void pwm_set_frequency(double freq_in_hz);

    /**
     * This method returns the actual PWM frequency.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return a real number representing the PWM frequency.
     */
    double pwm_get_frequency(bool from_buffer=false);

    /**
     * This method reads the encoder 1 count value.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return the encoder count value.
     */
    long int encoder_1_read_count(bool from_buffer=false);

    /**
     * This method reads the encoder 2 count value.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return the encoder count value.
     */
    long int encoder_2_read_count(bool from_buffer=false);

    /**
     * This method reads the encoder 1 time measurement between two
     * rising edges on CHA.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return a real number representing the time (in seconds).
     */
    double encoder_1_read_time(bool from_buffer=false);

    /**
     * This method reads the encoder 1 time measurement between two
     * rising edges on CHA.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return a real number representing the time (in seconds).
     */
    double encoder_2_read_time(bool from_buffer=false);

    /**
     * This method sets the encoder 1 positive direction. It has the
     * equivalent effect of swapping the encoder cables (CHA and CHB).
     * @param positive_direction it can be ENCODER_CW_IS_POSITIVE or
     *        ENCODER_CCW_IS_POSITIVE.
     */
    void encoder_1_set_direction(int positive_direction);

    /**
     * This method sets the encoder 2 positive direction. It has the
     * equivalent effect of swapping the encoder cables (CHA and CHB).
     * @param positive_direction it can be ENCODER_CW_IS_POSITIVE or
     *        ENCODER_CCW_IS_POSITIVE.
     */
    void encoder_2_set_direction(int positive_direction);

    /**
     * This method resets the encoder 1 count value.
     */
    void encoder_1_reset_counting();

    /**
     * This method resets the encoder 2 count value.
     */
    void encoder_2_reset_counting();

    /**
     * This method enables the encoder 1.
     */
    void encoder_1_enable();

    /**
     * This method enables the encoder 2.
     */
    void encoder_2_enable();

    /**
     * This method disables the encoder 1.
     */
    void encoder_1_disable();

    /**
     * This method disables the encoder 2.
     */
    void encoder_2_disable();

    /**
     * This method sets the encoder 1 quadrature.
     * @param quadrature it can be ENCODER_QUADRATURE_1X, ENCODER_QUADRATURE_2X or ENCODER_QUADRATURE_4X.
     */
    void encoder_1_set_quadrature(int quadrature);

    /**
     * This method sets the encoder 2 quadrature.
     * @param quadrature it can be ENCODER_QUADRATURE_1X, ENCODER_QUADRATURE_2X or ENCODER_QUADRATURE_4X.
     */
    void encoder_2_set_quadrature(int quadrature);

    /**
     * This method sets the timeout value of encoder 1. If no new pulse is received
     * within this time, the encoder will be considered stopped.
     * @param msecs is a positive integer representing the timeout in milliseconds.
     *        The smallest value is 0 and the maximum is 255.
     */
    void encoder_1_set_timeout(unsigned int msecs);

    /**
     * This method sets the timeout value of encoder 2. If no new pulse is received
     * within this time, the encoder will be considered stopped.
     * @param msecs is a positive integer representing the timeout in milliseconds.
     *        The smallest value is 0 and the maximum is 255.
     */
    void encoder_2_set_timeout(unsigned int msecs);

    /**
     * The encoder driver in the board has a moving average filter. The window size
     * can be changed using this method. It is only used to filter the time
     * measurements.
     *
     * @param size is a positive integer with maximum as 16 and minimum as 1.
     */
    void encoder_1_set_filter_size(unsigned int size);

    /**
     * The encoder driver in the board has a moving average filter. The window size
     * can be changed using this method. It is only used to filter the time
     * measurements.
     *
     * @param size is a positive integer with maximum as 16 and minimum as 1.
     */
    void encoder_2_set_filter_size(unsigned int size);

    /**
     * The encoder time measurement driver in the board has a relative rate limitation.
     * It is implemented the following algorithm:
     *
     *         (new_raw_measurement - old_measurement)
     * rate = -----------------------------------------
     *                  old_measurement
     *
     * If abs(rate) is bigger than max_rate, then the new measurement will be calculated as:
     *
     * new_measurement = (1 + sign(rate) * (max_rate / 100)) * old_measurement
     *
     * This prevents false readings due noisy signals or unbalanced encoders.
     *
     * @param max_rate a positive integer number between 0 and 200 (in %).
     */
    void encoder_1_set_rel_max_rate(unsigned int max_rate);

    /**
     * The encoder time measurement driver in the board has a relative rate limitation.
     * It is implemented the following algorithm:
     *
     *         (new_raw_measurement - old_measurement)
     * rate = -----------------------------------------
     *                  old_measurement
     *
     * If abs(rate) is bigger than max_rate, then the new measurement will be calculated as:
     *
     * new_measurement = (1 + sign(rate) * (max_rate / 100)) * old_measurement
     *
     * This prevents false readings due noisy signals or unbalanced encoders.
     *
     * @param max_rate a positive integer number between 0 and 200 (in %).
     */
    void encoder_2_set_rel_max_rate(unsigned int max_rate);

    /**
     * This method reads the firmware major version.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return a integer representing the major version.
     */
    int get_firmware_major_version(bool from_buffer=false);

    /**
     * This method reads the firmware minor version.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return a integer representing the major version.
     */
    int get_firmware_minor_version(bool from_buffer=false);

    /**
     * This method reads the firmware patch version.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return a integer representing the major version.
     */
    int get_firmware_patch_version(bool from_buffer=false);

    /**
     * This method reads the WHO_AM_I value.
     * @param from_buffer if true it will communicate with the board. If
     *        false it reads from the buffer.
     * @return it is expected to read the value 29.
     */
    int get_who_am_i();

    /**
     * This method closes the serial port.
     */
    void close();

protected:
    /**
     * Variable to store the registers values. All read and write operations
     * copy information into buffer.
     */
    u8 *buffer;

    dxl::InstructionPacket *instruction_packet;

    dxl::StatusPacket *status_packet;

    dxl::Receiver *receiver;

    /**
     * This function must be called when new bytes are available in
     * the serial port.
     *
     * @param data is a pointer to the new bytes.
     * @param size is how many bytes have arrived.
     */
    void receive_raw_data(u8 *data, u16 size);

    /**
     * This function is used to send a message. It does not send,
     * but it provides a pointer with the message data and how many
     * bytes need to be sent.
     *
     * @param data is a pointer to the message raw data.
     * @param size is how many bytes need to be sent.
     */
    void send_raw_data(u8 *data, u16 size);

    void receive_raw_data();

    void read_bytes(u8 first_register, u16 how_many);

    void write_bytes(u8 first_register, u8 *data, u16 how_many);

    void receive_status_packet();

    void decode_read_command();

    void decode_status_packet();

    template<typename type>
    void write_reg(Register<type>* reg)
    {
        write_bytes(reg->get_addr(), reg->get_pointer(), reg->get_total_size());
    }

    template<typename type>
    void read_reg(Register<type>* reg)
    {
        read_bytes(reg->get_addr(), reg->get_total_size());
    }

private:
    QSerialPort uart;

    int timeout_ms = 2;

    Register<u8>* GPIO_INPUT_STATE;
    Register<u8>* GPIO_INPUT_PULL;
    Register<u8>* GPIO_OUTPUT_STATE;
    Register<u8>* GPIO_OUTPUT_PULL;
    Register<u8>* GPIO_OUTPUT_MODE;
    Register<u8>* ADC_CONFIG;
    Register<u16>* ADC_FILTER_FREQ;
    Register<u16>* ADC_DATA_CH1;
    Register<u16>* ADC_DATA_CH2;
    Register<u16>* ADC_DATA_CH3;
    Register<u16>* ADC_DATA_CH4;
    Register<float>* PWM_FREQUENCY;
    Register<float>* PWM_DUTY_CH1;
    Register<float>* PWM_DUTY_CH2;
    Register<float>* PWM_DUTY_CH3;
    Register<float>* PWM_DUTY_CH4;
    Register<s32>* ENCODER_1_COUNT;
    Register<u32>* ENCODER_1_TIME;
    Register<s32>* ENCODER_2_COUNT;
    Register<u32>* ENCODER_2_TIME;
    Register<u8>* ENCODER_1_CONFIG;
    Register<u8>* ENCODER_2_CONFIG;
    Register<u8>* ENCODER_1_TIMEOUT_MS;
    Register<u8>* ENCODER_2_TIMEOUT_MS;
    Register<u8>* ENCODER_1_FILTER_SIZE;
    Register<u8>* ENCODER_2_FILTER_SIZE;
    Register<u8>* ENCODER_1_REL_MAX_RATE;
    Register<u8>* ENCODER_2_REL_MAX_RATE;
    Register<u8>* VERSION_MAJOR;
    Register<u8>* VERSION_MINOR;
    Register<u8>* VERSION_PATCH;
    Register<u8>* WHO_AM_I;

    void encoder_set_direction(int what_encoder, int direction);

    void encoder_reset_counting(int what_encoder);

    void encoder_enable(int what_encoder);

    void encoder_disable(int what_encoder);

    void encoder_set_quadrature(int what_encoder, int quadrature);

    void encoder_set_timeout(int what_encoder, unsigned int msecs);

    void encoder_set_filter_size(int what_encoder, unsigned int size);

    void encoder_set_rel_max_rate(int what_encoder, unsigned int max_rate);
};


#endif //QUARK_QUARK_H
