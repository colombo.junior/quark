#include "time.h"

u32 get_time_ms()
{
    u64 now = duration_cast<milliseconds>(time_point_cast<milliseconds>(high_resolution_clock::now()).time_since_epoch()).count();
    return static_cast<u32>(now - _tstart_ms);
}

u32 get_time_us()
{
    u64 now = duration_cast<microseconds>(time_point_cast<microseconds>(high_resolution_clock::now()).time_since_epoch()).count();
    return static_cast<u32>(now - _tstart_us);
}

void delay_ms(int time_to_wait)
{
    u64 now = get_time_us();
    while ((now + (1000 * time_to_wait)) > get_time_us()) {}
}

void delay_us(int time_to_wait)
{
    u64 now = get_time_us();
    while ((now + time_to_wait) > get_time_us()) {}
}
