Encoders
==========

The definitions herein are inside the **encoder** namespace.

Encoders are sensors mainly used to read the angular position of motors. These sensors usually have two digital
outputs (chA and chB). If the motor speed is constant, the phase between chA and chB is ideally 90º.

Quark firmware supports reading multiples encoders simultaneously. Two measurements are made:

-   **count the number of pulses**: depending if a clockwise (CW) or counterclockwise (CCW) rotation is detected,
    the pulses counting will be incremented or decremented one unity

-   **measure the pulses duration time (in micro-seconds)**: due manufacture reasons, the phase between chA and chB may
    not be 90º precisely. For this reason, the duration measures only the rise edges on chA


Pin definition
----------------

Up to two encoders can connected to the BluePill (STM32F103C8T6). The pins are:

.. table::
    :align: center

    +---------+------+------+
    | Encoder | chA  | chB  |
    +---------+------+------+
    |    1    | PA4  | PA5  |
    +---------+------+------+
    |    2    | PB10 | PB11 |
    +---------+------+------+


.. caution::

    It is very common that pull-up resistors need to be added to encoders chA and chB pins.
    Quark forces the MCU to add the internal (weak) pull-up resistors. Their resistances are relatively
    big (~30k) and then may not be suitable for long cabling needs. It is always recommended
    to add discrete resistors of adequate values.


.. important::

    Although the microcontroller logic level is +3.3V, accordingly to pages 26 - 30 of the
    `STM32F103x8 datasheet <https://www.st.com/resource/en/datasheet/stm32f103v8.pdf>`_,
    the pins of Encoder 2 (PB10 and PB11) are +5V tolerant. This allows the connection of +5V encoders.

The encoder pins are read and classified to Gray Code using :cpp:any:`encoder::Encoder::read_gpio`:

.. doxygenenum:: encoder::GrayCode
   :project: firmware


Count measurement
-------------------

When the logic state of an encoder pin changes, a measurement must be done. All the possible transitions are listed in
the table below:

.. table::
    :align: center

    +----------------------------+----------------------------+-------------------------+--------------+
    | From                       | To                         | Edges detected          | Event        |
    +============================+============================+=========================+==============+
    |                            | :cpp:any:`GRAY_0 <GRAY_0>` | None                    | Stopped      |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_1 <GRAY_1>` | Rising on chA           | CW increment |
    | :cpp:any:`GRAY_0 <GRAY_0>` +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_2 <GRAY_2>` | Rising on both          | Invalid      |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_3 <GRAY_3>` | Rising on chB           | CW decrement |
    +----------------------------+----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_0 <GRAY_0>` | Falling on chA          | CW decrement |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_1 <GRAY_1>` | None                    | Stopped      |
    | :cpp:any:`GRAY_1 <GRAY_1>` +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_2 <GRAY_2>` | Rising on chB           | CW increment |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_3 <GRAY_3>` | chB rises and chA falls | Invalid      |
    +----------------------------+----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_0 <GRAY_0>` | Falling on both         | Invalid      |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_1 <GRAY_1>` | Falling on chB          | CW decrement |
    | :cpp:any:`GRAY_2 <GRAY_2>` +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_2 <GRAY_2>` | None                    | Stopped      |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_3 <GRAY_3>` | Falling on chA          | CW increment |
    +----------------------------+----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_0 <GRAY_0>` | Falling on chB          | CW increment |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_1 <GRAY_1>` | chB falls and chA rises | Invalid      |
    | :cpp:any:`GRAY_3 <GRAY_3>` +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_2 <GRAY_2>` | Rising on chA           | CW decrement |
    |                            +----------------------------+-------------------------+--------------+
    |                            | :cpp:any:`GRAY_3 <GRAY_3>` | None                    | Stopped      |
    +----------------------------+----------------------------+-------------------------+--------------+

*   If a **CW increment** event is detected, then the count is incremented with 1
    :cpp:member:`encoder::Encoder::cw_increment` unit
*   If a **CCW increment** is detected, then the count is decremented with 1
    :cpp:member:`encoder::Encoder::cw_increment` unit
*   If an invalid or stopped event is detected, the count is not changed

The :cpp:member:`encoder::Encoder::cw_increment` signal (+ or -) can be chosen by the user by using the
:cpp:func:`encoder::Encoder::set_cw_is_positive` and :cpp:func:`encoder::Encoder::set_ccw_is_positive` functions.


.. note::

    The main causes for invalid movements detections are:

    * bad cabling (most common)
    * damaged encoder
    * the encoder is to fast for the board


Although the table bellow shows all the possible events, the user may choose to use only a few of them.
This setting is called quadrature and it is configured with the :cpp:func:`encoder::Encoder::set_quadrature`.
The available options are listed in the quadrature enum:

.. doxygenenum:: encoder::Quadrature
   :project: firmware


Time measurement
------------------

For safety reasons, the time measurement is always made with X1 quadrature (only on rising edges of chA).
This setting can not be override by the user without changing the library.

The time measurement may be trick to understand looking only for the code. For this reason, take a look
in the following figure:


.. image:: encoder_time_measurement_strategy.svg
   :width: 600px


As can be seen, the time measurement uses a timer. If the encoder is enabled, then all rising edges in
chA trigger the following procedure (implemented in :cpp:func:`encoder::Encoder::update_time`):

#.  An EXTI (pin interrupt) is triggered
#.  Immediately copy the actual timer count to an auxiliary variable
#.  Sum the copied value to the :cpp:member:`encoder::Encoder::duration_of_new_pulse_us`
#.  Copy :cpp:member:`encoder::Encoder::duration_of_new_pulse_us` to :cpp:member:`encoder::Encoder::duration_of_last_pulse_us`
#.  Multiply the actual timer count (auxiliary variable) by -1 and copy the result to
    :cpp:member:`encoder::Encoder::duration_of_new_pulse_us`

Moreover, in every timer overflow it is necessary to increment the overflow value (usually 1000 micro-seconds)
to the :cpp:member:`encoder::Encoder::duration_of_new_pulse_us`. This last procedure is implemented in
:cpp:func:`encoder::Encoder::update_on_overflow`.


Timeout
^^^^^^^^^

Any time measurement larger than :cpp:member:`encoder::Encoder::timeout_us` (in microseconds), will be finished and a
new time measurement will be started. By default the timeout is 100000 us (100 ms). This value can be changed with
:cpp:func:`encoder::Encoder::set_timeout` function.


Filtering
^^^^^^^^^^^

Soon!

Encoder enable and disable behaviour
--------------------------------------

Soon!

The Encoder class
-------------------

The encoder API is inside an namespace and organised in a class. The GPIO readings and IRQ settings are inside
dedicated functions. This allows this code to be easily ported to other platforms.

.. note::

    The private and public class concept is largely used in this API. Then, if a function or member is private,
    you should probably not be using it.


.. doxygenclass:: encoder::Encoder
    :project: firmware
    :private-members:
    :members:
    :undoc-members:




