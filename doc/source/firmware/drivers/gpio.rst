GPIO
======

Overview
----------

The board logic level is 3.3V and there are

- 4x input pins
- 4x output pins

Pull-up and pull-down resistors may be attached to the GPIO pins accordingly to the following table:

.. table::
    :align: center

    +-----------+-------------------------+
    | Direction | Pull resistor available |
    +===========+=========================+
    | Input     | Pull-UP and Pull-DOWN   |
    +-----------+-------------------------+
    | Output    | Pull-UP                 |
    +-----------+-------------------------+

The output pins can be configured to operate in push-pull or open drain mode.


Input pins
------------

The input pins are:

.. table::
    :align: center

    +---------------+---------------------+-----------------------+-------------+
    | Input channel | Microcontroller pin | Default pull resistor | 5V tolerant |
    +===============+=====================+=======================+=============+
    | CH1           | PA8                 | Pull-UP               | Yes         |
    +---------------+---------------------+-----------------------+-------------+
    | CH2           | PB15                | Pull-UP               | Yes         |
    +---------------+---------------------+-----------------------+-------------+
    | CH3           | PB14                | Pull-UP               | No          |
    +---------------+---------------------+-----------------------+-------------+
    | CH4           | PB13                | Pull-UP               | No          |
    +---------------+---------------------+-----------------------+-------------+


Output pins
-------------


The output pins are:

.. table::
    :align: center

    +----------------+---------------------+-----------------+
    | Output channel | Microcontroller pin | Maximum current |
    +================+=====================+=================+
    | CH1            | PC15                | 3mA             |
    +----------------+---------------------+-----------------+
    | CH2            | PC14                | 3mA             |
    +----------------+---------------------+-----------------+
    | CH3            | PC13                | 3mA             |
    +----------------+---------------------+-----------------+
    | CH4            | PB9                 | 25mA            |
    +----------------+---------------------+-----------------+

The output pins are in push-pull mode by default without pull resistor attached.



