Manager
=========

The manager is responsible for reading, writing, copying, measuring, etc. It
does not know how to read GPIO or encoders, but it knows how to process all
the events happening in the board. For example: when an instruction packet is
received, it will verify if the packet is valid, then process the instruction
and finally create and send a status packet.

Inside the :cpp:class:`Manager` class there are pointers to the following
objects:

* :cpp:class:`memory_table::MemoryTable`
* :cpp:class:`messenger::Messenger`
* :cpp:class:`encoder::Encoder`

These objects are declared in main_cpp file. After the manager is instantiated,
it is required to set the addresses of the above objects using the following
functions:

* :cpp:func:`Manager::set_memory_table_address`
* :cpp:func:`Manager::set_usb_messenger_address`
* :cpp:func:`Manager::set_encoders_address`

This procedure will also properly initialize them.

The Manager class is then presented.

.. doxygenclass:: Manager
    :project: firmware
    :private-members:
    :members:
    :undoc-members:
