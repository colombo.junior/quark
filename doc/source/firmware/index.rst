Firmware
==========


This is the firmware documentation, i.e., the C++ program that runs inside the microcontroller. It is divided in
modules, allowing code reuse.


.. toctree::
    :maxdepth: 1
    :caption: Contents:

    flashing/index
    manager/memory_table
    manager/manager
    drivers/gpio
    drivers/encoders
    communication/messenger
    communication/protocol
    dsp/dsp

