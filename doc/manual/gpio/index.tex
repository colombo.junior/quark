\chapter{GPIO - General Purpose I/O Pins}


\section{Overview}


The board logic level is 3.3~V and there are 

\begin{itemize}
    \item \nGPIOinputs{} input pins
    \item \nGPIOoutputs{} output pins
\end{itemize}

Pull-up and pull-down resistors may be attached to the GPIO pins accordingly to the following table:

\begin{table}[H]
    \centering 
    \begin{tabular}{cc}
        \hline 
        \textbf{Direction} & \textbf{Pull resistor available} \\
        \hline 
        Input     & Pull-UP and Pull-DOWN \\
        Output    & Pull-UP \\
        \hline 
    \end{tabular}
\end{table}

The output pins can be configured to operate in push-pull or open drain mode. 


\section{Input pins}


The input pins are:

\begin{table}[H]
    \centering 
    \begin{tabular}{ccc}
        \hline
        \textbf{Input pin} & \textbf{Microcontroller pin} & \textbf{Default pull resistor} \\
        \hline
        CH1                & PB8  & Pull-UP \\
        CH2                & PA8  & Pull-UP \\
        CH3                & PB15 & Pull-UP \\
        CH4                & PB14 & Pull-UP \\
        \hline
    \end{tabular}
\end{table}


\section{Output pins}


The output pins are:

\begin{table}[H]
    \centering 
    \begin{tabular}{cccc}
        \hline
        \textbf{Output pin} & \textbf{Microcontroller pin} & \textbf{Default mode} & \textbf{Default pull resistor} \\
        \hline
        CH1      & PC15 & Push-pull & No pull resistor \\
        CH2      & PC14 & Push-pull & No pull resistor \\
        CH3      & PC13 & Push-pull & No pull resistor \\
        CH4      & PB9  & Push-pull & No pull resistor \\
        \hline
    \end{tabular}
\end{table}


\newpage 


\section{Registers}

\subsection{GPIO\_INPUT\_STATE - Input pin logic state}


The pins logic state is automatically updated 
% by means of interrupts triggered by rising or falling edges.
every 1~ms. If the logic level in the input pin is HIGH, then 1 is written to its position in this register. 


\begin{table}[H]
    \setlength{\arrayrulewidth}{1.5pt}
    \setlength\extrarowheight{5pt}
    \centering
    \begin{tabular}{L{2cm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}}
        \multicolumn{1}{l}{Bit} & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 \\ \cline{2-9} 
        \multicolumn{1}{l|}{0x00} & 
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{-}} & 
        \multicolumn{1}{c|}{\textbf{-}} & 
        \multicolumn{1}{c|}{\textbf{-}} & 
        \multicolumn{1}{c|}{\textbf{CH4}} & 
        \multicolumn{1}{c|}{\textbf{CH3}} & 
        \multicolumn{1}{c|}{\textbf{CH2}} & 
        \multicolumn{1}{c|}{\textbf{CH1}} \\ \cline{2-9} 
        \multicolumn{1}{l}{Access} & R & R & R & R & R & R & R & R \\ 
        \multicolumn{1}{l}{Initial value} & 0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 
    \end{tabular}
\end{table}


\subsection{GPIO\_INPUT\_PULL - Input pins pull resistors configuration}


Pull-up and pull-down resistors may be attached to the input GPIO pins. This setting prevents the pins to be floating and then avoiding false readings. By default the input pins have the pull-up resistor enabled. 


\begin{table}[H]
    \setlength{\arrayrulewidth}{1.5pt}
    \setlength\extrarowheight{5pt}
    \centering
    \begin{tabular}{L{2cm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}}
        \multicolumn{1}{l}{Bit} & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 \\ \cline{2-9} 
        \multicolumn{1}{l|}{0x01} & 
        \multicolumn{2}{c|}{\textbf{CH4}} & 
        \multicolumn{2}{c|}{\textbf{CH3}} & 
        \multicolumn{2}{c|}{\textbf{CH2}} & 
        \multicolumn{2}{c|}{\textbf{CH1}} \\ \cline{2-9} 
        
        \multicolumn{1}{l}{Access} & 
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} & 
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} \\ 
        
        \multicolumn{1}{l}{Initial value} & 
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{1} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{1} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{1} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{1} \\ 
    \end{tabular}
\end{table}


For each channel (CHx), the possible pull resistors configuration is shown in the table below:

\begin{table}[H]
    \centering
    \caption{Possible pull-up and pull-down resistors configurations.}
    \label{tab:PUPDX}
    \begin{tabular}{|C{15mm}|C{15mm}|l|}
        \toprule
        \textbf{CHx (MSB)} & \textbf{CHx (LSB)} & \textbf{Description} \\
        \midrule
        0 & 0 & No pull-up and no pull-down \\
        0 & 1 & Pull-up enabled \\
        1 & 0 & Pull-down enabled\\
        1 & 1 & Reserved \\
        \bottomrule
    \end{tabular}
\end{table}


\subsection{GPIO\_OUTPUT\_STATE - Output pins logic state}


The board output pins operate in push-pull mode. 


\begin{table}[H]
    \setlength{\arrayrulewidth}{1.5pt}
    \setlength\extrarowheight{5pt}
    \centering
    \begin{tabular}{L{2cm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}}
        \multicolumn{1}{l}{Bit} & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 \\ \cline{2-9} 
        \multicolumn{1}{l|}{0x02} & 
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{CH4}} & 
        \multicolumn{1}{c|}{\textbf{CH3}} & 
        \multicolumn{1}{c|}{\textbf{CH2}} & 
        \multicolumn{1}{c|}{\textbf{CH1}} \\ \cline{2-9} 
        
        \multicolumn{1}{l}{Access} & 
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} \\ 
        
        \multicolumn{1}{l}{Initial value} & 
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} \\ 
    \end{tabular}
\end{table}


\textbf{CHx} control the logic level of each output pin: 1 set high (+3.3~V) and 0 set low (0~V). Although the 4 MSB bits are writable, they are reserved and have no effect. 


% \begin{block}
%     Caution! Because the output GPIO pin operates in push-pull mode, short-circuits may burn the pin. 
% \end{block}

\newpage

\subsection{GPIO\_OUTPUT\_PULL - Output pins pull resistor configuration}

This register controls what pull resistors are attached to the output GPIO pins. By default no pull resistor is attached. 

\begin{table}[H]
    \setlength{\arrayrulewidth}{1.5pt}
    \setlength\extrarowheight{5pt}
    \centering
    \begin{tabular}{L{2cm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}}
        \multicolumn{1}{l}{Bit} & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 \\ \cline{2-9} 
        \multicolumn{1}{l|}{0x03} & 
        \multicolumn{2}{c|}{\textbf{CH4}} & 
        \multicolumn{2}{c|}{\textbf{CH3}} & 
        \multicolumn{2}{c|}{\textbf{CH2}} & 
        \multicolumn{2}{c|}{\textbf{CH1}} \\ \cline{2-9} 
        
        \multicolumn{1}{l}{Access} & 
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} & 
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} \\ 
        
        \multicolumn{1}{l}{Initial value} & 
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} \\ 
    \end{tabular}
\end{table}

For each channel (CHx), the possible pull resistors configurations are shown in the table below:

\begin{table}[H]
    \centering
    \caption{Possible pull-up and pull-down resistors configurations.}
    \label{tab:GPIO_OUTPUT_PULL_OPTIONS}
    \begin{tabular}{|C{15mm}|C{15mm}|l|}
        \toprule
        \textbf{CHx (MSB)} & \textbf{CHx (LSB)} & \textbf{Description} \\
        \midrule
        0 & 0 & No pull-up and no pull-down \\
        0 & 1 & Pull-up enabled \\
        1 & 0 & Pull-down enabled\\
        1 & 1 & Reserved \\
        \bottomrule
    \end{tabular}
\end{table}


\subsection{GPIO\_OUTPUT\_MODE - Output pins mode}

This register controls the operation mode of the output pins: push-pull or open-drain.

\begin{table}[H]
    \setlength{\arrayrulewidth}{1.5pt}
    \setlength\extrarowheight{5pt}
    \centering
    \begin{tabular}{L{2cm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}C{15mm}}
        \multicolumn{1}{l}{Bit} & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 \\ \cline{2-9} 
        \multicolumn{1}{l|}{0x04} & 
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{-}} &
        \multicolumn{1}{c|}{\textbf{CH4}} & 
        \multicolumn{1}{c|}{\textbf{CH3}} & 
        \multicolumn{1}{c|}{\textbf{CH2}} & 
        \multicolumn{1}{c|}{\textbf{CH1}} \\ \cline{2-9} 
        
        \multicolumn{1}{l}{Access} & 
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} &  
        \multicolumn{1}{c}{R/W} \\ 
        
        \multicolumn{1}{l}{Initial value} & 
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} &  
        \multicolumn{1}{c}{0} \\ 
    \end{tabular}
\end{table}

The operation mode options are described in the following table. Although the 4 MSB bits are writable, they are reserved and writing to them has no effect. 

\begin{table}[H]
    \centering
    \caption{Possible pull-up and pull-down resistors configurations.}
    \label{tab:GPIO_OUTPUT_MODE_OPTIONS}
    \begin{tabular}{|C{15mm}|l|}
        \toprule
        \textbf{CHx} & \textbf{Description} \\
        \midrule
        0 & Push-pull \\
        1 & Open-drain \\
        \bottomrule
    \end{tabular}
\end{table}




